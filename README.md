# ~/ABRÜPT/ÉTIENNE MICHELET & DONIA JORNOD/MĀRA/*

La [page de cet antilivre](https://abrupt.cc/michelet-jornod/mara/) sur le réseau.

## Sur l'antilivre

Māra démon masculin et calculateur tente de s’introduire dans les rêves de Kusul la fille perle. Elle résiste pacifiquement, ou non. Essai théorique, aphorismes, conte, fantasmagorie gore, poème ou pamphlet ésotérique. Tout à la fois.

Et l'écho du charbon, au travers des œuvres de Donia Jornod, pour s'enfouir sous les cendres de l'esprit, et y placer l'image d'une résistance.

## Sur l'autrice et l'auteur

<a href="https://doniajornod.org" target="_blank" class="shake">Donia Jornod</a> est apparue en 1991 avec les prémices du réseau. Artiste hologramme située à Zürich, ses œuvres côtoient les monstres en deçà des perceptions.

<a href="https://www.dongmuri.net/" class="shake" target="_blank">Étienne Michelet.</a> Né en 1984. Vit en Corée du Sud.
Écrit un journal, avec animaux, lune, fleurs et des fantômes aussi. Enregistre sa voix, parfois, pour y voir mieux.

## Sur la licence

Cet [antilivre](https://abrupt.cc/antilivre/) est disponible sous licence Creative Commons Attribution – Pas d’Utilisation Commerciale – Partage dans les Mêmes Conditions 4.0 International (CC-BY-NC-SA 4.0).

## Etc.

Vous pouvez également découvrir notre [site](https://abrupt.cc) pour davantage d'informations sur notre démarche, notamment quant au [partage de nos textes](https://abrupt.cc/partage/).
