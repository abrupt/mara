---
author: "Étienne Michelet"
illustrator: "Donia Jornod"
title: "Māra"
publisher: "Abrüpt"
date: 2021
depot: "quatrième trimestre"
description: ""
subject: ""
lang: "fr"
identifier:
- scheme: 'ISBN-13'
  text: '978-3-0361-0136-1'
rights: "© 2021 Abrüpt, CC BY-NC-SA"
adresse: "https://abrupt.cc/michelet-jornod/mara"
miroir: ""
binaire: |
---
