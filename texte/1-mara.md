\cleardoublepage

\includepdf[width=180mm,height=230mm]{gabarit/images/1.jpg}

\includepdf[width=180mm,height=230mm]{gabarit/images/1bis.jpg}

\thispagestyle{empty}
\vspace*{9\baselineskip}

\thispagestyle{empty}
\dictum[Simone Weil]{Il faut accrocher son désir à l'axe des pôles.}

\cleardoublepage


\asterismeun

Non, je ne veux pas \textsc{tuer les fantômes}. Cette violence, il faudrait
qu'elle sorte nettement, comme la Dionée se ferme sur la mouche.

\asterismedeux

Je suis environné d'esprits, que je le veuille ou non. Alors j'invente
des scories \textsc{volcaniques}.

\asterismetrois

On fait de la poésie pour bébés, une \textsc{versification prénatale} et
nauséabonde. Māra salive la crasse de l'humanité, mais \textsc{cette pourriture}
n'a pas d'odeur, ni de parfum. Elle pue dans les têtes.

\asterismequatre

Māra, ton sourire purulent je vais en démanteler le \textsc{système} métrique
\textsc{jusqu'à l'os}. Cracher et me taire, ou ne jamais me taire.

\asterismecinq

Māra est sorti d'un morceau de canne à sucre. Māra avait trois filles
\textsc{qui étaient toutes} Māra. Elles dansaient le jour, la nuit... elles
dansaient jusqu'à la \textsc{nausée}.

\asterismesix

Dieu, Dieu, Dieu... la lune va luire pour te nuire, alors mâche encore
des feuilles de sophora, \textsc{c'est mieux que} la scammonée. Dans les
abattoirs les vaches, les veaux, tu sais ? On les égorge de \textsc{sang-froid},
mon cœur...

\asterismesept

Endormissement, \textsc{parasomnie}, quand ils me parlaient, ce n'est pas à moi
qu'ils parlaient... Pendant que les autres \textsc{parlaient, moi je ne parlais pas}, je collectionnais des escargots.

\asterismehuit

Aux quatre coins de la rose des vents s'en vont les \textsc{animaux rampants}.

\asterismeun

Faire offrande, c'est se \textsc{couper une main}.

\asterismedeux

Māra a toutes ses \textsc{saisons} en enfer. Māra se loge dans les têtes et dans
la poitrine. Il griffe la membrane \textsc{oculaire}. Tout en souriant il se
\textsc{décroche les dents}, il te les enfonce dans les \textsc{tympans}, il te vomit dans
les cheveux, te frictionne le crâne gaiement d'une \textsc{main convulsive}.

\asterismetrois

Māra est une ordure, il veut que tu sois ordure. Sa vérité d'ordure veut
te briser, t'écraser \textsc{les vertèbres}, en faire des colonnes de douleur, à
en faire hurler les loups. Māra veut brûler la jungle, Māra veut jouir
\textsc{dans le feu}.

\asterismequatre

Il y a tellement de feuilles qui sortent de tes yeux. Māra crachera dans
tes yeux pour les arroser... Māra affamé léchera ta nuque, caressera tes
aisselles comme s'il s'agissait \textsc{d'oiseaux fragiles}.

\afterpage{
  \thispagestyle{empty}
\includepdf[width=180mm,height=230mm]{gabarit/images/2.jpg}
}

\asterismecinq

Il arracherait bien la tête de ces oiseaux sortis du nid pour la mâcher
lentement, et surtout les yeux, car Māra adore manger des yeux, en faire
de la bouillie, un festin de tous les diables, un caprice \textsc{de
mastication}...

\asterismesix

Le mont *Bukhansan* est un refuge pour les fantômes et les esprits. J'y
ai vu Dakini danser sur un seul pied, puis ouvrir les cuisses et montrer
son \textsc{vagin humide et bleu}.

\asterismesept

Dakini souriait avec des lèvres bleues et tout près d'elle un \textsc{loup
mangeait l'aisselle} d'une autre femme. \textsc{Des crânes} brûlaient et des
flammes montaient des crânes et léchaient \textsc{les pieds} de Dakini... Son
front était aussi \textsc{un sexe bleu}, et la fleur sous son pied était une
flamme bleue... et sa danse était elle aussi une flamme bleue.

\asterismehuit

Māra *nightmare*, si on ne comprend plus rien à ce que tu dis, c'est à
cause du prurit de tes dents... tes filles sont des goules aux pieds de
vagins vahinés.

\asterismeun

Māra que tu es laid... je suis renard, je maraude l'esprit léger... je
passe des heures les yeux fermés \textsc{sans saliver}. Un chien dort sur du
fumier, c'est ma seconde naissance... \textsc{dans l'acuité} ! Argos... la nuit
étoilée vibre \textsc{comme des mouches} autour de tes yeux, viens que je te
lèche le visage \textsc{comme un fin limier}... tu sais bien que les dieux \textsc{sont
des moments} de l'homme...

\asterismedeux

Le masculin Māra surjoue sa vigueur. Priape palefroi et palefrenier
tout à la fois, il donne \textsc{des coups de boutoir} à te fracasser les reins.
Māra bande \textsc{nerveusement}, ce trousseur de filles est en secret
\textsc{collectionneur de larves}.

\asterismetrois

\textsc{Je te désire} Kusul, ma perle, bien \textsc{autrement}. Mon désir n'est pas mort
crois-moi, ton corps je le désire comme une fleur le \textsc{moro-sphynx}.

\asterismequatre

Les infamies avaient conflué dans mon cœur comme des excréments dans des
latrines. Je décuple \textsc{ma sauvagerie}, *subséquemment*... Je cesse de
parler et j'assassine, c'est le \textsc{b.a.-ba}... Je vais leur raboter les
paupières... à ces mânes aux doigts câlins. Ce n'est pas un simple
ornement, \textsc{je touche vraiment des fantômes}.

\asterismecinq

C'est le début de la fin, ou la fin du début, touche-toi les orteils
\textsc{trois fois} Māra et aspire à la mort spirituelle, dans un écho, un écho,
un écho...

\asterismesix

Les plafonds deviennent tous \textsc{de la boue}. J'ai un tunnel dans la tête, on
y cherche des passages coûte que coûte...

Cette pénétration morale \textsc{insidieuse} est immorale... Faisons bouillir le
chevreau dans le lait de sa mère... \textsc{Pourquoi pas} ?

\asterismesept

Je partialise tes fesses Kusul et je crache sur la société \textsc{de la
psychiatrie}. Où sont tes fétiches Māra ? Kusul, ton pied lui est
spirituel... Et je lécherai sur tes talons tout le \textsc{poison de ce monde}.
Je me fous bien des communautés, je n'aime que ton corps à toi, \textsc{et ton
con}.

\asterismehuit

\textsc{Les hommes au pouvoir} se promènent et ne cachent plus leur érection.
Objet de psychiatrie, \textsc{la norme-coït} construit la clinique psychiatrique.
Les médecins \textsc{ont baisé} la monopathologie. Les articulations sont usées
*psychologiquement*. Māra, ton éthique normative \textsc{de cancrelat}...

\afterpage{
  \thispagestyle{empty}
\includepdf[width=180mm,height=230mm]{gabarit/images/3.jpg}
}

\asterismeun

*Psychopathia sexualis*, mon œil ! quand ils parlent de perversion, de
dégénérescence... Nous sommes \textsc{asymétriques} en amour Kusul, c'est sûr,
mais cette grande majorité d'hommes qui bandent dans les
cliniques-bordels, menaçons-les avec une faucille, on nous poursuivra
avec \textsc{des chiens et des drones}...

\asterismedeux

Māra, viens un peu que je t'arrache les dents, que je les \textsc{expulse} comme
de gros diamants, pour en faire des colliers de séton rouge. Je vais te
disséquer, séance tenante, \textsc{sans regret}...

\asterismetrois

Et toi barbi·e barbante de l'inclusive, fille de Māra, goule
capricieuse, ta face sous toutes les coutures, on la voit trop souvent
\textsc{dans ce manège} qui t'occupe vivement, les fruits des corbeilles
imprégnés des rides de tes aisselles, la poésie \textsc{soumise} à tes dents
cariées.

\asterismequatre

Et toi, goule poétique sentimentale, as-tu lu quelque chose \textsc{pour que ta
cervelle} suinte aussi fort la mort, et avec tant de ferveur ? Ton cœur
brisé en miettes mielleuses, ta face de vieille femme, tes cheveux \textsc{de
veau} et ton corps tatoué jusqu'aux pis...

\asterismecinq

Les égéries purulentes, poète·sse·s de la \textsc{transgression des normes} qui
font de la poésie une aberration normative. Je m'inquiète, la poésie,
elles l'ont soumise au déclin.

\asterismesix

Kusul, nous faisons \textsc{laboratoire} dans nos têtes depuis l'enfance. Tes
joues, ta mâchoire, je les embrasserais jusqu'à ne plus avoir de
souffle...

\asterismesept

\textsc{Pas de lieu autre que le lieu sans lieu}. Kusul, je voudrais te parler de
vie surnaturelle. \textsc{Sous tes yeux}, Māra est \textsc{dégluti}. C'est un fait,
l'oiseau va vivre en lui becquetant les yeux.

\asterismehuit

Regarde bien mes yeux Kusul, cerclés de noir, \textsc{de points rouges}. Je
pourrais crever de rire dans les rues de Séoul. Un jour, la terre
s'est mise à trembler. Même \textsc{mes lèvres} remuaient !

\asterismeun

Et voilà que je me casse le nez. \textsc{Le sang inonde la chambre}. Dorénavant,
le réel n'est plus \textsc{immaculé}. Kusul, veux-tu un peu de rhinoplastie ?
\textsc{L'os propre} de mon nez me fait un nez busqué, je suis pâle comme un
\textsc{renard}...

\asterismedeux

Kusul et ta mémoire... *La descañona*, elle l'écorche... \textsc{Mauvaise
nuit}, bien mauvaise, à vomir, ni plus ni moins. *Le sommeil de la
raison* en quelque sorte... Chauves-souris et hiboux, un lynx dans le
coin droit, et des \textsc{bébés morts} dans des paniers tressés. \textsc{Les femmes
sucent} les gamins jusqu'à la moelle. Suivez les conseils du Grand Bouc
! *Volaverunt*...

\asterismetrois

Kusul, je veux faire *Zazen*. Parce que ma \textsc{cervelle} a trop vrillé. Kusul
\textsc{colle ta langue} sous ton palais humide. Tes doigts, tes mains
s'immobilisent. L'air vibre comme une flamme bleue. Ton cœur ne saute
plus \textsc{comme un lapin}...

\asterismequatre

Ne penses-tu pas que tout était \textsc{prévisible} ? Portez vos
énormes \textsc{cornes de licornes} antiques... on vous ouvrira la bouche, on
vous fera la chasse aux dents. C'est magnifique cette sanguine, cette
face \textsc{de pendu}, quelle volupté du tracé ! Quelles fesses de marbre ! On
voudrait les palper, les mordre. Brûlons la \textsc{maison cocotte}. Ils s'en
iront déplumés, ils s'en iront *Zazen* les sortilèges, la bave aqueuse
de Māra, tu vas vite flairer l'épine \textsc{petite hermine}, et traverser les
mers.

\asterismecinq

Capturer des fantômes, est-ce bien sérieux Kusul ? En position \textsc{du lotus},
avec mon nez cabossé ? On fait de la poésie dans des livres cousus main !
Cette \textsc{aliénation de la langue}, leur face farcie d'eau. Quels
passionnants résultats ! Bête originale, au paroxysme de la crise,
j'éternue !

\asterismesix

Pointe d'une aiguille d'acupuncture que l'on m'enfonce \textsc{dans les joues},
ou sur la langue ? Non, plutôt dans les genoux ! Ou sous la voûte
plantaire... \textsc{Plantaire} !

\asterismesept

Respiration \textsc{abdominale} abominable, comme le vent souffle ! Il fait
remuer les branches... Les branches remuent comme des os souples. Des
\textsc{poulpes souples}...

\asterismehuit

\afterpage{
\clearpage
\thispagestyle{empty}
\includepdf[width=180mm,height=230mm]{gabarit/images/4.jpg}
}

Des fleurs en intraveineuse Kusul, elles fleurissent dans ton sang...
Des fleurs blanches comme des \textsc{globules en grappes}... Des reliquaires
mous !

\asterismeun

Est-ce que ces fleurs de sang pourraient guérir \textsc{les fibroses}, calmer,
fleurir sur les nerfs ?... Les fleurs, Kusul, fleurissent simplement...
Écoute comme le hautbois sonne \textsc{haut dans les bois}. Peut-être que des
serpents dorment sous les planchers, dissimulés dans l'ombre, fuyant les
humains \textsc{comme la peste}.

\asterismedeux

Peut-être que se sont les serpents qui jouent du hautbois ! L'humain est
si occupé, plongé dans \textsc{sa morbidité} quotidienne, asphyxié \textsc{dans le
néant}... Serpents, crachez... *Sam xiag, sam xiag, sam xiag*... Crachez,
mais surtout... \textsc{ne touchez pas} à cette main humaine ! Kusul, toi aussi,
fuis l'humain...

\asterismetrois

Il faudrait donc entrer en résonance avec son époque, \textsc{son naufrage} ?
Mais non, mais non... Quand le sang coulait de mes narines, j'essayais
de retenir le \textsc{liquide hématique} dans le creux de ma main, mais il y en
avait trop ! Que voulais-je retenir à la fin ? Ce n'est pas sérieux ? On
ne peut \textsc{assujettir} le sang !

\asterismequatre

Kusul, tu te mettras à prier, qui sait, devant les images sculptées de
*Sansin*, divinité de la montagne, accompagnée de son tigre \textsc{aux yeux
hallucinés}...

\asterismecinq

Ne perds pas l'espoir \textsc{de te perdre}. Tu l'as bien vu quand tu as fermé
les yeux, le tigre noir s'évapore. Tu lui as offert quelques fraises de
tes mains fiévreuses. \textsc{Ses poils} se hérissaient, puis il a ouvert \textsc{la
gueule} pour te parler :

*L'araignée serra le cœur et le cœur serra l'araignée. Le cœur et
l'araignée s'aimèrent et l'araignée et le cœur ne se quittèrent plus.
L'araignée caressa le cœur de ses huit pattes fragiles et le cœur
réchauffa l'araignée de son sang rouge. Il n'y a pas de piège dans cet
amour-là*.

\asterismesix

Pour sûr Kusul, ce tigre te ressemble, il parle \textsc{la même langue que toi}.
Peut-être que cet animal, comme un double \textsc{fantomatique}, te parle parce
que \textsc{tu es muette}.

\asterismesept

Aujourd'hui, tu regardes les feuilles qui remplacent les feuilles.
Personne n'enlève \textsc{la peau des choses}, tu le sais très bien. Gautama est
silencieux. Pourquoi vouloir à tout prix faire sens, ou faire trembler
\textsc{l'œil du lyrisme} ?

\asterismehuit

Tu préfères tisser, gonfler la tapisserie de ses motifs végétaux,
animaux comme la langue secrète \textsc{éructée} dans le coquillage.

\asterismeun

Mais ma parole, Kusul, tu parles ! Mais oui tu parles et tu commences à
mettre cet alphabet \textsc{dans ta bouche} de fantôme. *Hangeul*. Peut-être
as-tu une spiritualité débordante, un \textsc{monument infini} dans la
cervelle...

\asterismedeux

Māra, ta bouche pâteuse aux relents de vin espagnol... mes yeux
tremblent à ce mot, \textsc{société}... alors... y \textsc{foutre le feu}, une bombe
artisanale s'il le faut, allumer un incendie sur la \textsc{société blanche,
intellectuelle}, les saigner, les auteurs poétiques du partage
\textsc{d'immondices}.

\asterismetrois

Kusul, détends-toi un peu... Ça t'apaise de poser ton regard sur \textsc{le
sommeil des chiens. Le mouvement autonome du vivant} n'a besoin d'aucune
parole pour \textsc{unifier}. La vie devenue visible, \textsc{hypervisible} est une
négation de la vie. Il faut donc défendre \textsc{l'invisible}.

\afterpage{
  \thispagestyle{empty}
\includepdf[width=180mm,height=230mm]{gabarit/images/5.jpg}
}

\asterismequatre

Tu ne veux aucun mal, \textsc{aucune plaie}. Plus d'hôpitaux, \textsc{d'effondrements}, de
punaises, de poissons d'argent (\textsc{lépisme}), de cafouillage, de tombeaux,
d'engloutissements sociaux... Avec le vent, \textsc{tout deviendra horizontal}.

\asterismecinq

Au lieu de colonies \textsc{d'icebergs}, réduis ta pensée à la taille d'un
flocon saisonnier. Ce qui \textsc{coagule} n'est que formulations négatives...
Les éclairs sont des mondes qui entrent dans le monde.

\asterismesix

Métaphysique de Kusul. Kusul pense et écrit : *Le romantisme d'époque
s'agite dans une \textsc{fantasmagorie révolutionnaire et idiote}. Son
impuissance est contenue dans ses dithyrambes d'un siècle révolu et qui
vibrionne \textsc{sans substance} comme des relents de dialectique dans une
sphère numérique sans fond.*

\asterismesept

*Son manque de substance s'accorde à la \textsc{nébulisation} de l'information
et du langage. Son vice et sa faiblesse élaborent une pensée rétrograde
qui annihile \textsc{toute force} véritablement créatrice et condamne l'homme à
graviter dans un \textsc{immobilisme} délétère et stérile.*

\asterismehuit

*Le monde poursuit ainsi \textsc{ses mutations dévastatrices}, celles ordonnées
par une société ultrapositiviste dont le moteur de \textsc{productivité} est
alimenté par \textsc{une chambre de combustion} d'où se désagrège et brûle la
volonté créatrice d'émancipation.*

\asterismeun

*C'est la destruction de toute volonté \textsc{humaine et créatrice} qui met en
branle la société ultrapositiviste. Les pseudo-révoltes sont les
\textsc{aliments inoffensifs} du monstre carnassier. L'anéantissement de la
pensée est son principe de \textsc{production élémentaire}. Les cervelles sont
les combustibles pour l'effacement même de la pensée.*

\asterismedeux

On y foutrait le feu Kusul, à ce monde qui sent \textsc{la pisse de bœuf}. Les
racines sèchent et se brisent comme des os canins. \textsc{La vie se fossilise}.
Alors, qu'en penses-tu de ce monde en ruines ? Tu sais, moi je ne vois
pas les ruines... Je suis sonné, le \textsc{coup résonne} sorcière, mon
secret... la *mètis*... chien fourbe, panthère ou renard rusé. Je me
dissimule, j'affuble en esprit... c'est la conduite \textsc{de celui qui connaît
la vérité}.

\asterismetrois

Mais avant de mettre ne serait-ce qu'un seul pied dans le sanctuaire...
Prenons un bon \textsc{bain de pieds} ! Et lavons-nous \textsc{les globes oculaires} à
l'acide ! Parce que nous allons voir des tortues noires \textsc{qui s'accouplent}
avec des serpents tortueux. Leurs langues ne formant qu'une seule flamme
dans un baiser en \textsc{brasier rouge} sang.

\asterismequatre

Nous verrons aussi... les mutations rouges, les corps en fusion, les
oreilles en plumes, \textsc{les cartilages noueux}. Les créatures se
métamorphoseront \textsc{dans le feu}. Vraiment !

\asterismecinq

La langue est une chambre de combustion où le feu régénère le monde,
elle le \textsc{purifie}. Vous qui puez comme on \textsc{pue du crâne}, vos pensées
s'écroulent, il ne reste plus que vos yeux qui fixent le jour, vos
gorges, ou des \textsc{nombrils cicatrisés}, purulents. Vous \textsc{égorgez} les animaux.
Vos pensées sont laborieuses, elles sortent d'un trou terreux où
macèrent \textsc{les racines d'agave}.

\asterismesix

Oui Māra, tu \textsc{mords l'agave}, jusqu'à te teindre les gencives \textsc{d'un vert
affreux}... à se tordre de rire, oui... Le vert d'agave dans \textsc{ta bouche
terreuse}.

\afterpage{
  \thispagestyle{empty}
\includepdf[width=180mm,height=230mm]{gabarit/images/6.jpg}
}

\asterismesept

Longtemps, très longtemps, j'ai eu un esprit lunaire, dévoré,
\textsc{dévorant}...mais un jour je me suis mis à aimer \textsc{les pagodes} de pierre,
sans raison, inconditionnellement, irrationnellement.

Je rêvais des tombes de *Goguryeo*, \textsc{d'un bestiaire de sang}, des gardiens
de l'enfer \textsc{aux yeux} de lotus. Le serpent tortueux, tu ne le vois donc
pas ? C'est le nerf lui-même, ou \textsc{la décharge nerveuse}.

\asterismehuit

Je fermais les yeux, je les voyais ces fleurs \textsc{éclore dans l'obscurité}.
Où est-elle ton harmonie, renard doux, \textsc{panthère des sables} ? Doux renard
de \textsc{la viduité}. Je restais à l'intérieur, car l'extérieur \textsc{n'était pas
fiable}. À cultiver mes pensées, comme de l'agave.

\asterismeun

Kusul pense encore : *L'homme est retenu par sa pensée sur l'homme, et
il n'en sort pas*. Mieux vaut ne pas avoir \textsc{le cœur trop fragile}, Kusul,
sinon clairement tu te feras bouffer. Ne vois-tu pas qu'ils sont
incapables d'écouter, qu'ils agissent ou ne réagissent qu'en fonction de
\textsc{ressentiments} animés par leur esprit de mort ? Leur existence est vidée
de toute substance \textsc{ou même de volonté}. Ils ont besoin de t'exclure,
comme ils excluent \textsc{la réalité du monde}. Ils sont dépourvus d'imaginaire
ou d'intuition.

\asterismedeux

Leurs rêves sont rares et absurdes. Sinon des rêves de \textsc{vengeance}... Ils
peuvent te détester comme au fond ils se détestent \textsc{eux-mêmes}, jusqu'à
\textsc{s'automutiler} ! Se détester est encore une conséquence de leur
\textsc{narcissisme pathologique}.

\asterismetrois

Ils détestent le monde et les autres, mais ne sont \textsc{jamais rien} sans
cette détestation des autres. Cette pathologie nerveuse est invisible au
premier abord, bien que \textsc{les yeux les trahissent}, quand ils se figent,
\textsc{cette gelée oculaire}...

\asterismequatre

Kusul, cette gelée oculaire, tu la vois dans les regards figés par \textsc{la
torpeur}, dans leurs yeux hagards... Ils semblent te mitrailler de leur
sempiternel questionnement : \textsc{qui suis-je} ? Dis-le-moi je t'en supplie,
toi, que je ne connais pas, que je ne pourrai jamais connaître et qui
n'existes que pour me dire ce que je suis... \textsc{J'ai besoin de toi}, pour me
dire ce que je suis. Je ne peux pas le faire \textsc{seul·e}. Quand je suis
seul·e, \textsc{je n'existe pas}. Ma solitude ne m'apprend rien. Et
malheureusement, cette \textsc{méconnaissance du monde}, en dehors de ma sphère
personnelle et vide, m'ennuie. Je m'ennuie...

\asterismecinq

Tant pis pour eux. Au fond, les \textsc{miroirs noirs} combleront d'images ce
vide existentiel profond. Leur regard sera *fixé*. Leur regard sera dopé
\textsc{au vide}. Ils sont dans les nœuds de la mort, *Tssing Tssing*, Kusul
décoche une flèche... tu es vraiment belle avec ton arc, ton \textsc{œil
meurtrier}... et ta vengeance qui \textsc{émascule}. Vas-y ! Chasse crûment !
Sonde mon cœur si tu veux, et \textsc{torpille la glaire}.

\asterismesix

As-tu remarqué Kusul comme la langue peut circuler et prendre à
contre-pied \textsc{cette inertie} du langage ? C'est un contrepoint qui orne,
comme une veillée dans le vivant, et qui vient \textsc{occulter} la torpeur
\textsc{ankylosée} des névropathes.

\asterismesept

Tu as beaucoup pensé à ça Kusul, et tu continues : *La reconnaissance
intuitive est \textsc{une rupture}, à la fois faille et éveil. Dans
l'insignifiance du langage, elle devient \textsc{signifiant aporétique}.*
Il faut tenter une pensée ni ascendante ni descendante, mais une
\textsc{dialectique des os}, du grain et de la moelle : une \textsc{métaphysique
intracorporelle}. Ta métaphysique voudrait définir cette intuition de
vie dissimulée et invisible, comme une énergie organique qui t'anime et
qui peut être \textsc{source de rêves} ou de réminiscences de vies antérieures.

\asterismehuit

Ta colonne vertébrale est \textsc{un totem} d'où jaillissent des couleurs
irisées. \textsc{Ce signifiant} reste toujours à la limite, à la frontière de la
perception et se dérobe à la faculté des sens. Cette matière sans fond,
comme \textsc{l'antimatière d'un songe}, *incarne* le mystère en se refusant à la
\textsc{mortification du langage} et des images. Tu en perçois le flux intérieur
et mouvant dans un espace indéfini et intime.

\afterpage{
  \thispagestyle{empty}
\includepdf[width=180mm,height=230mm]{gabarit/images/7.jpg}
}

\asterismeun

Vie intérieure ?... Sûrement plus que cela... Vie \textsc{surnaturelle} ! La
mémoire remue comme le dos d'un poisson... Cette mise en branle \textsc{secoue}
ton être, tes pensées deviennent \textsc{poreuses} et s'interpénètrent au sein
d'un même présent \textsc{qui brûle}. Kusul, tu es habitée par tes rêves...

\asterismedeux

La langue s'accélère et s'accélère, ou bien \textsc{est-ce le cœur} ? Cette
sensation, une voix qui monte et qui monte dans la tête. J'ai vu passer
l'automne à la vitesse d'un \textsc{avion de guerre}. Ma tête crépitait dans les
bruits des moteurs, et je voulais parler seul... Je renâcle ? Mais avant
l'*homophrosunè*, il y a le meurtre des prétendants, le \textsc{sang noir} qui
coule sur les dalles de la maison et les gorges qui crachent leur mort.
Dans les râles, tu m'enlaces Kusul, ma perle, même si je pue \textsc{le sang et
la sueur}.

\asterismetrois

Les apitoiements pour la masse aveugle \textsc{des aveugles}. *Papaver,
papaver*... Joyeux jardin des bulbes, danse des trombones et des
cuivres... Tubas et glaives à la fois... On peut voir \textsc{saturne} cette nuit
et ses sœurs Alcyonides... C'est toi Kusul qui me la montres du doigt...
Puisque nous sommes tout juste \textsc{au soir du solstice}, nous mangerons une
soupe de haricots rouges !

\asterismequatre

Il y a un vent salubre qui nettoie des alluvions du passé. Ce vent, je
le sens, il me foudroie, je le reçois \textsc{en plein visage}. Le regard n'a
jamais été émoussé. Dans mes yeux, il n'y a pas d'erreur. Nous aurons,
Kusul et moi, des yeux plus grands que des soucoupes. Parce qu'il y a un
vent de \textsc{tous les dieux}.

\asterismecinq

Je t'écoute parler Kusul... ce langage m'enchante... \textsc{Les rhizomes} se
déploient à vue d'œil. Les branches montent géométriquement. Les fruits
mûrissent. Alcyon couve ses œufs \textsc{en silence}, puisque nous venons juste
de passer le solstice. L'œuf et le cœur, \textsc{ces noyaux calmes}, bien loin de
Māra.

\asterismesix

J'ai un bambou affuté dans les mains... \textsc{Affuté} ! Tu auras double dose
Māra, si je te croise, je te planterai le bambou \textsc{dans la gorge}, pour que
ton sang inonde le sol et qu'il te monte à la tête. La plaie sera
immense, ce sera un joli \textsc{trou noir}, comme l'écorce du crime qui accroche
\textsc{l'épiderme}. Élection divine d'un démon ! Tes yeux te sortent de la tête.
Tire la langue, que \textsc{je te la tranche} ! Cette langue, empoisonnée comme
une feuille de sang, je la jette au sol et je l'écrase, je la piétine,
comme \textsc{une éponge de viande}. Cette sangsue humaine, poison et source de
poison.

\asterismesept

J'en ai trop vu de ces goules mâcher puis roter leurs coquillages. Et
vous, vous ne voudriez pas angoisser \textsc{face à la mastication} des
coquillages ? Les pavots étoilés s'ouvrent comme des giroflées, \textsc{leurs
yeux sont des points crevés}. Il faudrait dormir Kusul, main dans la
main, et parler comme \textsc{des enfants dyslexiques}. *Anansi*, je te mangerai
la tête le premier, je te ferai cuire des ormeaux et des coquillages de
saison pour tes mâchoires endolories. \textsc{Magie du meurtre} dans les champs
d'Amathonte, *mais tout change et rien ne meurt.*

\asterismehuit

Je tiens le divin bambou. Je vais te l'enfoncer dans les cavités
oculaires, faire sortir de ton ventre \textsc{une colonie de papillons}. Sur les
toits d'émail, \textsc{on sacrifie} des oiseaux, des plantes, des tigres. \textsc{On
brûle leurs yeux}, on lisse leurs plumes dans des bassins de terre et
d'argile. On remuera des cils, on glacera \textsc{les ventricules et les artères
coronaires}, tu ne crois pas ?

\asterismeun

Dans les cages thoraciques, dans les courants d'air, qui les remarquera
cette fois Kusul, tes baisers *halal*, ta \textsc{frilosité mentale} ?

\asterismedeux

Je joue des gammes \textsc{sur des os de cobra}, j'ai ouvert mon cœur je te jure,
en deux ou trois : il était frais, il était beau comme un fruit avec ses
graines de pavot, \textsc{ses écailles de truite} et ses alvéoles tirées au
cordeau. Phaéton, fils idiot, \textsc{qu'on t'immole}, qu'on te plante des
couteaux dans les cuisses, esprit de graisse et de cire, qu'on te brûle
paille-en-queue \textsc{jusqu'à la moelle}.

\asterismetrois

Māra va sucer les cornes des vaches cornues jusqu'à ce que \textsc{nos larmes
s'évaporent}. Mentalement, je me calme en nourrissant les anguilles du lac
Pergus, en nourrissant les carpes du lac Averne... Mais que vois-je ? On
te coupe la gorge ? On te renverse \textsc{dans la tourbe}, dans le sang. C'est
mérité *Lycaon*, \textsc{mâcheur de doigts}, singe hurleur et mangeur de
hachis... de \textsc{dawamesk}, on te gave, mais rien n'apaise ta fièvre.

\asterismequatre

Kusul, jouons tous les deux allongés sur des tapis de fibres, à chercher
puis à trouver des fleurs \textsc{dans les motifs}, je te dirai ce que je vois...
Je t'invite à bâfrer une bonne salade d'amome, je sais \textsc{couper} les
feuilles en fines lamelles, c'est ma spécialité, de \textsc{manier le couteau},
de hacher menu les végétaux pour en retirer le suc, l'amertume...

\afterpage{
  \thispagestyle{empty}
\includepdf[width=180mm,height=230mm]{gabarit/images/8.jpg}
}

\asterismecinq

Je caresse \textsc{tes phalanges} Kusul, c'est une évidence, ton sang qui
circule entre les os saillants et se verse comme le courant d'une
rivière \textsc{entre les pierres}... Mais voici les ombres qui s'avancent, \textsc{les
yeux cireux} et tristes comme des poulpes crevés sous le poids des
tentacules qui sonnent le glas, des testicules tatoués ou des anneaux
nasaux, quand \textsc{on épingle les papillons} pour faire des partitions
célestes, mais moi \textsc{j'aime tes os}.

\asterismesix

La psychologie du philosophe s'atrophie sous psychotropes... et acidifie
mes vertiges... Kusul, toi seule tu adoucis les angles de la géométrie,
et \textsc{tu sucres les fraises} par poignées fébriles \textsc{sur les têtes imbéciles},
ceux qui baignent dans l'esthétique de la neurasthénie.

\asterismesept

Kusul me raconte une histoire :

*Au bord du fleuve, un singe mange des organes. Le sang lui barbouille
la face en rouge solaire. Les babouins embrassent les antilopes, défiant
la mâchoire du crocodile, mais où sont donc passées les fleurs ?*

Et puis une autre :

*Deux pastèques et deux rats qui s'approchent en silence dans la terre
et crèvent les pastèques le jus dégouline sur la terre et sur les dents
des rats qui mordent dans les pastèques qui se crèvent et se vident les
rats mâchent les grains de pastèque dans la nuit on entend le bruit des
grains qui éclatent dans la nuit et les yeux des rats sont aussi des
grains noirs comme ceux de la pastèque la lune brille dans la nuit et
brille aussi dans les yeux noirs des rats et les pastèques coulent et le
jus rouge se déverse sur la terre et les pattes des rats
s'enfoncent dans la boue rose et sucrée l'un des rats monte sur le dos
de l'autre rat pour forniquer dans la nuit sous la lune qui brille dans
le ciel et dans les yeux des rats mais le rat forniqueur se fait mordre
par l'autre rat qui veut continuer de creuser la chair sucrée de la
pastèque qui coule sur la terre humide mais l'ombre d'un chien errant
apparaît sous la lune alors les deux rats s'enfuient dans la nuit le
chien se met à renifler la pastèque et les traces laissées dans la
terre humide par les rats il lèche la chair de pastèque tombée sur le
sol et creuse la terre et pisse sur les pastèques et les feuilles de
pastèque.*

\asterismehuit

La scolopendre tu vois, c'étaient mes propres doigts qui cherchaient
\textsc{quelque chose à aimer}... la scolopendre tu vois, c'étaient mes propres
doigts qui pondaient leurs œufs sur la tige d'un rosier... Māra roule
des yeux terribles, sa mâchoire rumine une colère rouge. Māra, c'est le
bruit de l'époque. Māra, c'est \textsc{le ventre} des images. Ses filles sont
obscènes, non par le corps, mais par \textsc{le masque} de la tromperie et des
idées rances. Il y a de la fatigue dans leurs yeux, posés sur une bouche
qui parle seule.

\asterismeun

Les goûts douteux, \textsc{la mastication} qui creuse la joue. Les filles de Māra
tournent sur elles-mêmes \textsc{comme des vers}. Nous sommes bien loin d'une
communion spirituelle. Elles prennent une figure de circonstance, afin
de creuser \textsc{une terreur émotionnelle}. Elles salivent sous les masques de
la musique et de la danse. \textsc{Leurs narines sèches} se dilatent. Elles
veulent provoquer \textsc{la mort spirituelle}, toucher au corps vide,
désincarné, morbide.

\asterismedeux

Mais toi, qui voudrait véritablement entendre ce que tu as à dire, Kusul
? Qui veut encore entendre ta voix ? \textsc{Et pourquoi parler} ? Tu ne veux pas
de cette parole qui se déverse \textsc{jusqu'à la céphalée}. Elles se sont donné
des noms de fleurs, mais tu le sais très bien... les fleurs véritables
sont \textsc{les fleurs muettes}. Et les filles de Māra crient, mais ignorent que
tu ne les entends pas... Voudraient-elles \textsc{ensemencer la mer}, là où les
graines ne peuvent germer ?

\asterismetrois

Dans les filles de Māra, n'oublie pas que c'est \textsc{le masculin morbide qui
s'incarne seul}. C'est la pensée sans voile, le rire de \textsc{la masturbation
électrique}. Māra te masturbera de force, plongera ses mains entre tes
cuisses Kusul, il \textsc{humera} ton sexe comme s'il lui revenait \textsc{de droit}. Sa
langue glisse sur ses lèvres. Les filles de Māra meurent de faim en te
dévisageant avec \textsc{leur sexe qui est aussi une bouche}. Le voici, l'amour
surdoré de la digestion stomacale. Ton \textsc{anéantissement} est leur
nourriture.

\asterismequatre

Leurs seins en pointe ne sont que \textsc{le membre érectile} et vert de Māra
leur père. Elles veulent sucer tes doigts comme elles suceraient la
mort, d'une succion affamée, prêtes à mordre. \textsc{À découper la chair}. Ton
anéantissement, leur nourriture... Cette panique, \textsc{ces révulsions} du
corps, Kusul, tu aurais pu les prendre pour une vérité, parce qu'elles
te sautent au visage, alors que la vérité, elle, sait patienter. Les
filles de Māra ne sont que des *images*, des perturbations dues à Māra,
conséquences de son existence \textsc{malheureuse} dont il fait \textsc{une prédation}...
La prédation est sa réponse au malheur. Il cherche ainsi \textsc{une proie} pour
que la mastication du vivant \textsc{couvre le silence} de sa mort.

\asterismecinq

Mais le silence de mort n'est pas identique \textsc{au silence du vivant}. Le
silence peut manifester toutes les strates du vivant (liquides,
aériennes, terrestres...) \textsc{Et tu le sais}. L'immobilité contient toutes
les directions possibles du mouvement. \textsc{L'immobilité peut résister} au
silence de la mort. Elle stimule silencieusement la racine, \textsc{le suc
interne} des floraisons. Elle fonde une force, une puissance invisible
contenue dans le cœur du monde, c'est-à-dire \textsc{dans le vide}. Qu'on puisse
la nommer... peu importe, puisque cette essence \textsc{défie le langage} et la
reconnaissance des sens. Si l'on peut espérer assimiler son rythme
périodiquement, rien ni personne \textsc{ne peut la contraindre}. C'est un cœur
sans matière, un cercle qui n'a pas de centre, ou \textsc{une source secrète},
dissimulée mais omniprésente. C'est à la fois \textsc{la peau de toute chose},
son contenu matériel et sa substance immatérielle...

\asterismesix

Plus que les corps, les ombres retiennent ton attention Kusul. On a
voulu te faire croire que les chauves-souris étaient des créatures
maléfiques. \textsc{Il n'en est rien}, et tu peux les chérir, les caresser, fixer
leurs yeux noirs \textsc{sans avoir honte}. De toute façon, il y a bien longtemps
que tu ne fais plus partie de cette humanité adepte \textsc{de la mastication}.
Mais de quoi est fait ce regard, celui qui te donne à voir \textsc{un monde qui
entre dans le monde} ? Où places-tu ton *locus amoenus*, Kusul ? Dans le
ciel, je crois...

\afterpage{
  \thispagestyle{empty}
\includepdf[width=180mm,height=230mm]{gabarit/images/9.jpg}
}

\asterismesept

Jamais plus elles ne verront ton visage, ces sorcières des rites. \textsc{Ces
suceuses de vertèbres}. Pourrais-tu être sensible Kusul au chant des
oiseaux et oublier l'odeur de la viande sanglante des bébés cuits \textsc{dans
des rivières} ? Leurs danses macabres, leurs rires mortifères ?
Aujourd'hui, le ciel était clair comme de l'eau \textsc{dans une bassine}.

\asterismehuit

Je ne cille pas. Oh, mon Dieu, je ne pleure pas. Surtout pas de ça avec
moi ! Je suis hanté. Je nous revois autour de cette table, entre ces
murs. \textsc{Coupe-moi la tête}. Arrache-moi cette face de rat ! Ma langue fait
\textsc{trop cactus}, ma langue de reptile. À la place, j'aurais dû, si j'avais
su, si j'avais su... m'enfuir. Quitte à m'envoyer des doses de
mescaline, ou faire pousser de l'agave dans une serre, \textsc{tester des
décoctions} pour tout noter dans un carnet. J'aurais eu comme ça les yeux
grands ouverts. Ouverts sur des couleurs, \textsc{des symétries folles} des
infinis multipliés. De belles tapisseries en définitive.

\asterismeun

Une cabane, un mirage... Elle grouille de sauterelles noires... \textsc{Rien n'y
tient} lieu de refuge. Ses parois tremblent. Cabane de boue, de courants
d'air toxique. Les paroles grasses y infusent. C'est \textsc{la torpeur du
banal}, des dates, des calendriers, des éphémérides bientôt ridés,
bariolés, \textsc{de cuivre} plus que d'or. On y glousse, on y tousse grassement
les glaires laborieuses. Le plafond suinte de dégoût, la pluie
insalubre. Les cheveux humides \textsc{de sueur capillaire} plus que de pluie.
Les araignées se décomposent dans les fumées. Non, ce n'est pas un
refuge, la cabane dégringole, et j'échappe de justesse \textsc{à l'écrasement},
en passant par la fenêtre...

\asterismedeux

Je te jure Kusul, c'est magnifique, le lion d'Yvain leur saute au
visage, \textsc{les défigure}... d'un coup de mâchoire emporte le flanc d'un
homme. Les filles de Māra seront aussi \textsc{tuées} par un lion dans mon rêve,
croquées jusque dans les viscères... moi, je les finirai à l'arme
blanche.

\asterismetrois

Parce que je reste assis immobile et que je ne dis rien et que \textsc{je ne
pense plus}, si ce n'est à une manière de ne plus penser et de ne plus
regarder, ne plus écouter sinon \textsc{l'ampleur} d'un souffle qui s'accorde au
silence... Māra, le silence, l'immobilité sereine, ça te tue.

\asterismequatre

Je me fous du cosmos, de l'Olympe, du mont des Oliviers, mais peut-être
pas tellement du \textsc{figuier des pagodes}. J'essaie de tenir mon délire, \textsc{de
délier} le spirituel, de faire vibrer la dentition.

\asterismecinq

Il faut, pour parler de ça, l'avoir connue cette brûlure dans la
poitrine, ce resserrement dans l'abdomen. \textsc{Nous tournons
elliptiquement} dans ce monde Kusul, et nous revenons aux saisons qui ne
sont jamais tout à fait les mêmes. Et nous non plus, nous ne serons
jamais tout à fait les mêmes. C'est en cela que \textsc{cette vie est parfaite}.
Tu dois aimer les cercles, les cycles, l'elliptique. Tout peut renaître
dans ton cœur, si tu gardes cette volonté parfaite.

\asterismesix

Ceux qui sont unis... \textsc{sans raison}... n'ont aucune raison pour se
séparer... Ne cherche pas à me comprendre... Nous sommes des mystères...
*plus haut que l'amour des hommes est l'amour des choses et des
fantômes. Ce fantôme qui court devant toi, il est plus beau que toi ;
pourquoi ne lui donnes-tu pas ta chair et tes os ? Mais tu as peur et tu
cours te réfugier auprès de ton prochain.* J'aurais donné ma peau pour
\textsc{des fantômes de quartz} ! Pour des fantômes de sang, des yeux de
fantômes, des fantômes de fleurs...

\asterismesept

Nous sommes toujours des fantômes \textsc{en rêve}. Nous ouvrons nos yeux de
fantômes sur \textsc{la nuit intérieure}.

\asterismehuit

Je te reparle des *gui* Kusul, ces fantômes errants, ces ombres noires échevelées. J'ai vu un *gui* un jour, \textsc{alors que j'étais enfant}, un après-midi \textsc{étrangement silencieux}... j'étais seul dans un jardin, je me tenais debout face à cette \textsc{silhouette noire}. Ses cheveux électriques semblaient danser. Je me souviens très bien de ses yeux ronds qui me fixaient comme deux points blancs. Pourtant, \textsc{je n'ai pas eu peur}... Dans le silence, le fantôme, \textsc{tout en me fixant} avec ses yeux blancs, s'est mis à me sourire, au milieu \textsc{des fleurs de cosmos}.

\cleardoublepage

\includepdf[width=180mm,height=230mm]{gabarit/images/10bis.jpg}

\includepdf[width=180mm,height=230mm]{gabarit/images/10.jpg}

