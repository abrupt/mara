// Scripts

// Correct 100 Vh - Source : https://codepen.io/team/css-tricks/pen/WKdJaB
// First we get the viewport height and we multiple it by 1% to get a value for a vh unit
let vh = window.innerHeight * 0.01;
// Then we set the value in the --vh custom property to the root of the document
document.documentElement.style.setProperty('--vh', `${vh}px`);

// We listen to the resize event
window.addEventListener('resize', () => {
  // We execute the same script as before
  let vh = window.innerHeight * 0.01;
  document.documentElement.style.setProperty('--vh', `${vh}px`);
});

// Prevent bounce effect iOS
// Source https://gist.github.com/swannknani/eca799795860cff222f70b8675f8c8d8
// var content = document.querySelector('.home');
// content.addEventListener('touchstart', function (event) {
//   this.allowUp = this.scrollTop > 0;
//   this.allowDown = this.scrollTop < this.scrollHeight - this.clientHeight;
//   this.slideBeginY = event.pageY;
// });

// content.addEventListener('touchmove', function (event) {
//   var up = event.pageY > this.slideBeginY;
//   var down = event.pageY < this.slideBeginY;
//   this.slideBeginY = event.pageY;
//   if ((up && this.allowUp) || (down && this.allowDown)) {
//     event.stopPropagation();
//   } else {
//     event.preventDefault();
//   }
// });

// Variables & tools & functions
let zIndexDrag = 1;
let firstTime = 0;
let musiquePlay = false;
const menuTetes = document.querySelectorAll('.image--tete');
let allAnim = [];

function randomNb(min, max) { // min and max included
  return Math.floor(Math.random() * (max - min + 1) + min);
}

function shuffle(array) {
  for (let i = array.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [array[i], array[j]] = [array[j], array[i]];
  }
}

function moveFrame(el) {
  let x = (((Math.random() < 0.5 ? -1 : 1) * randomNb(10,40)) / 100) * document.documentElement.clientWidth;
  let y = (((Math.random() < 0.5 ? -1 : 1) * randomNb(10,40)) / 100) * document.documentElement.clientHeight;
  anime({
    targets: el,
    translateX: x,
    translateY: y,
    duration: randomNb(1000,2000),
    easing: 'easeInOutCubic',
    complete: function(anim) {
      el.setAttribute('data-x', anim.animations[0].currentValue.slice(0, -2));
      el.setAttribute('data-y', anim.animations[1].currentValue.slice(0, -2));
    }
  });
}

function moveAround(el){

  const mediaQuery = window.matchMedia('(min-width: 50em)')
  if (mediaQuery.matches) {
    x = (Math.random() < 0.5 ? -1 : 1) * randomNb(100,300);
    y = (Math.random() < 0.5 ? -1 : 1) * randomNb(100,300);
  } else {
    x = (Math.random() < 0.5 ? -1 : 1) * randomNb(50,200);
    y = (Math.random() < 0.5 ? -1 : 1) * randomNb(50,200);
  }
  // Random zIndex
  if (firstTime < nuages.length) {
    el.style.zIndex = randomNb(1,99);
    move(el, nuage = true);
    firstTime++;
  }
  else {
    move(el, nuage = true);
  }

};

// function move(el) {
//   anime({
//   targets: el,
//   easing: 'linear',
//   translateX: x,
//   translateY: y,
//   duration: randomNb(2000,4000),
//   easing: 'easeInOutCubic',
//   complete: function(anim) {
//     el.setAttribute('data-x', anim.animations[0].currentValue.slice(0, -2));
//     el.setAttribute('data-y', anim.animations[1].currentValue.slice(0, -2));
//   }
// });
// }

const mediaQuery = window.matchMedia('(min-width: 50em)')
let temps = 30000;
if (mediaQuery.matches) {
  temps = 30000;
}
else {
  temps = 10000;
}
let easing = 'linear';

function move(el, nuage = false) {
  if (nuage) {
    function randomValues() {
      let animation = anime({
        targets: el,
        left: function(anim) {
          let screenWidth = window.innerWidth
          || document.documentElement.clientWidth
          || document.body.clientWidth;
          let posX = randomNb(0, screenWidth) - anim.clientWidth;
          if (posX < 0) posX = posX + anim.clientWidth;
          return posX;
        },
        top: function(anim) {
          let screenHeight = window.innerHeight
          || document.documentElement.clientHeight
          || document.body.clientHeight;
          let posY = randomNb(0, screenHeight) - anim.clientHeight;
          if (posY < 0) posY = posY + anim.clientHeight;
          return posY;
        },
        easing: easing,
        duration: temps,
        complete: function(anim) {
          randomValues();
          // el.setAttribute('data-x', anim.animations[0].currentValue.slice(0, -2));
          // el.setAttribute('data-y', anim.animations[1].currentValue.slice(0, -2));
        }
      });
      allAnim.push(animation);
    }
    randomValues();
  }
  else {
    anime({
    targets: el,
    translateX: x,
    translateY: y,
    duration: randomNb(2000,4000),
    easing: 'easeInOutCubic',
    complete: function(anim) {
      el.setAttribute('data-x', anim.animations[0].currentValue.slice(0, -2));
      el.setAttribute('data-y', anim.animations[1].currentValue.slice(0, -2));
    }
    });
  }
}

  // // x = (Math.random() < 0.5 ? -1 : 1) * randomNb(40,100);
  // // y = (Math.random() < 0.5 ? -1 : 1) * randomNb(40,100);
  // // var widthPixels = el.clientWidth;
  // var screenWidth = window.screen.width;
  // var percentage = ( screenWidth - el.clientWidth ) / screenWidth ;
  // // console.log(percentage);
  // x = randomNb(0,80);
  // y = randomNb(0,80);
  // let animCloud = anime.timeline({
  // targets: el,
  // easing: easing,
  // // duration: randomNb(1000,2000),
  // duration: temps,
  // // easing: 'easeInOutCubic',
  // // loop: true,
  // complete: function(anim) {
  //   // move(el);
  //   // el.setAttribute('data-x', anim.animations[0].currentValue.slice(0, -2));
  //   // el.setAttribute('data-y', anim.animations[1].currentValue.slice(0, -2));
  //   // console.log(anim)
  // },
  // update: function(anim) {
  // }
// }).add({
  // // translateX: 10,
  // // translateY: 10,
  // top: x + '%',
  // left: y + '%',
  // update: function(anim) {
  //   el.setAttribute('data-x', anim.animations[0].currentValue.slice(0, -2));
  //   el.setAttribute('data-y', anim.animations[1].currentValue.slice(0, -2));
  // }
  // });
  // allAnim.push(animCloud);

// Textes
let containerOpen = false;

const textesContainer = document.querySelector('.textes');
const textesContainerEffect = document.querySelector('.textes--effect');
const textes = document.querySelectorAll('.texte');
let textesListe = [];
for (const texte of textes) {
  textesListe.push(texte);
}

function shuffleTextes() {
  textesListe[0].classList.remove('appear');
  shuffle(textesListe);
  textesContainer.classList.add('show');
  textesContainer.style.zIndex = 100 + zIndexDrag;
  for (const item of textesListe) {
    item.classList.remove('show');
  }
  textesListe[0].classList.add('show');
  moveFrame(textesContainer);
  void textesListe[0].offsetWidth;
  textesListe[0].classList.add('appear');
  textesListe[0].scrollIntoView({block: "start", inline: "start"});
}

// Images
const imagesContainer = document.querySelector('.images');
const images = document.querySelectorAll('.image--mara');
let imagesListe = [];
for (const image of images) {
  imagesListe.push(image);
}

function shuffleImages() {
  imagesListe[0].classList.remove('appear');
  shuffle(imagesListe);
  imagesContainer.classList.add('show');
  imagesContainer.style.zIndex = 100 + zIndexDrag;
  for (const item of imagesListe) {
    item.classList.remove('show');
  }
  imagesListe[0].classList.add('show');
  moveFrame(imagesContainer);
  void imagesListe[0].offsetWidth;
  imagesListe[0].classList.add('appear');
}

// Close
const closeBtn = document.querySelector('.btn--close');
closeBtn.addEventListener('click', (e) => {
  e.preventDefault();
  nuages.forEach((el) => moveAround(el));
  containerOpen = false;
  textesContainer.classList.remove('show');
  textesContainer.removeAttribute('style');
  textesContainer.removeAttribute('data-x');
  textesContainer.removeAttribute('data-y');
  imagesContainer.classList.remove('show');
  imagesContainer.removeAttribute('style');
  imagesContainer.removeAttribute('data-x');
  imagesContainer.removeAttribute('data-y');

  const audios = document.querySelectorAll('audio');
  for (let i = 0; i < audios.length; i++) {
    audios[i].pause();
  }
  closeBtn.classList.add('none');
  musiqueBtn.classList.add('none');
  closeBtn.blur();
});

// Musique
// var musiqueJoue = "pause";
let vol = 0;
let interval = 30;
let fadeInAudio;
let fadeOutAudio;
function musique(track) {
  if (track.paused) {
    clearInterval(fadeInAudio);
    clearInterval(fadeOutAudio);
    track.volume = vol;
    track.play();
    fadeInAudio = setInterval(function () {
    if (track.volume < 0.99) {
        track.volume += 0.01;
    }
    else {
      clearInterval(fadeInAudio);
    }
    }, interval);
  } else {
    track.pause();
  }
}

// Menu
const menuBtn = document.querySelector('.btn--menu');
const menu = document.querySelector('.menu');
// const fondSonore = document.getElementById('fond-sonore');
const musiqueBtn = document.querySelector('.btn--musique');
const musiqueBtnOn = document.querySelector('.btn--musique .info--on');
const musiqueBtnOff = document.querySelector('.btn--musique .info--off');
let menuOpen = false;

menuBtn.addEventListener('click', (e) => {
  e.preventDefault();
  menuOpen = !menuOpen;
  menu.classList.toggle('show--menu');

  menuTetes.forEach((tete) => {
    x = (Math.random() < 0.5 ? -1 : 1) * randomNb(40,200);
    y = (Math.random() < 0.5 ? -1 : 1) * randomNb(40,200);
    move(tete);
  });
  menuBtn.blur();
});

menuTetes.forEach((tete) => {
  tete.addEventListener('click', (e) => {
    x = (Math.random() < 0.5 ? -1 : 1) * randomNb(40,200);
    y = (Math.random() < 0.5 ? -1 : 1) * randomNb(40,200);
    move(tete);
  });
});

let silence = false;
musiqueBtn.addEventListener('click', (e) => {
  e.preventDefault();
  const audios = document.querySelectorAll('audio');
  if (silence) {
    musique(audios[randomNb(0, 7)]);
  } else {
    clearInterval(fadeInAudio);
    clearInterval(fadeOutAudio);
    for (let i = 0; i < audios.length; i++) {
    audios[i].pause();
    }
  }
  musiqueBtnOff.classList.toggle('none');
  musiqueBtnOn.classList.toggle('none');
  silence = !silence;
  musiqueBtn.blur();
});

let xPos = 0;

const nuages = document.querySelectorAll('.nuage');
let x;
let y;
for (const nuage of nuages) {
  Pace.on('hide', function () {
    moveAround(nuage);
  });

  const son = nuage.nextElementSibling;

  nuage.addEventListener('click', (e) => {
    e.preventDefault();
    closeBtn.classList.remove('none');
    musiqueBtn.classList.remove('none');
    // console.log(allAnim);
    // allAnim.forEach((el) => el.remove(el.animatables[0].target));
    containerOpen = true;
    allAnim.forEach((el) => el.remove(el.animatables[0].target));
    shuffleImages();
    shuffleTextes();
    const audios = document.querySelectorAll('audio');
    for (let i = 0; i < audios.length; i++) {
      if (audios[i] != son) {
        audios[i].pause();
      }
    }
    if (!silence) {
      // musiqueBtnOff.classList.toggle('none');
      // musiqueBtnOn.classList.toggle('none');
      // silence = !silence;
      // if (son.paused) {
      //   musique(son);
      // }
    // } else {
      if (son.paused) {
        musique(son);
      }
    }
    nuage.blur();
    for (const nuage2 of nuages) {
      if (nuage2 != nuage) {
        x = (Math.random() < 0.5 ? -1 : 1) * randomNb(40,200);
        y = (Math.random() < 0.5 ? -1 : 1) * randomNb(40,200);
        move(nuage2);
      }
    }
  })
}

const frames = document.querySelectorAll('.textes, .images');
for (const frame of frames) {
  frame.addEventListener('click', (e) => {
    for (const frameMov of frames) {
      x = (Math.random() < 0.5 ? -1 : 1) * randomNb(40,200);
      y = (Math.random() < 0.5 ? -1 : 1) * randomNb(40,200);
      move(frameMov);
      for (const nuage of nuages) {
        x = (Math.random() < 0.5 ? -1 : 1) * randomNb(40,200);
        y = (Math.random() < 0.5 ? -1 : 1) * randomNb(40,200);
        move(nuage);
      }
    }
  });
};

// let imageObjects = [];

// function loadImages(images, onComplete) {
//   let loaded = 0;
//   function onLoad() {
//       loaded++;
//       if (loaded == images.length) {
//           onComplete();
//       }
//   }
//   for (let i = 0; i < images.length; i++) {
//     let img = new Image();
//     img.addEventListener("load", onLoad);
//     img.src = images[i];
//     imageObjects.push(img);
//   }
// }

// const canvas = document.querySelector('.area');
// const ctx = canvas.getContext('2d');

// const widthCanvas = canvas.width = window.innerWidth;
// const heightCanvas = canvas.height = window.innerHeight;

// // function to generate random number

// function random(min, max) {
//   const num = Math.floor(Math.random() * (max - min + 1)) + min;
//   return num;
// }

// // define Ball constructor

// function Ball(x, y, velX, velY, img, widthImg, heightImg) {
//   this.x = x;
//   this.y = y;
//   this.velX = velX;
//   this.velY = velY;
//   this.img = img;
//   this.width = widthImg;
//   this.height = heightImg;
// }

// // define ball draw method
// Ball.prototype.draw = function() {
//   ctx.beginPath();
//   ctx.fillStyle = this.color;
//   ctx.drawImage(this.img, this.x, this.y, this.width, this.height);
// };

// // define ball update method

// Ball.prototype.update = function() {
//   if((this.x + this.width) >= widthCanvas) {
//     this.velX = -(this.velX);
//   }

//   if((this.x - this.width) <= 0) {
//     this.velX = -(this.velX);
//   }

//   if((this.y + this.height) >= heightCanvas) {
//     this.velY = -(this.velY);
//   }

//   if((this.y - this.height) <= 0) {
//     this.velY = -(this.velY);
//   }

//   this.x += this.velX;
//   this.y += this.velY;
// };

// // define array to store balls and populate it
// let balls = [];
// let imagesSrc = [];

// let images = document.querySelectorAll('.image');
// images.forEach((img) => {
//   imagesSrc.push(img.src)
// })
// // for (let i = 0; i < 8; i++) {
// //   let src = "/img/nuages/nuage" + i + ".png";
// //   imagesSrc.push(src);
// // }

// function createObject() {
//   for (let i = 0; i < 8; i++) {
//     let widthImg = 300;
//     let ratio = imageObjects[i].width / widthImg;
//     let heightImg = imageObjects[i].height / ratio;
//     let ball = new Ball(
//       // (widthCanvas/2) - (widthImg/2),
//       // (heightCanvas/2) - (heightImg/2),
//       random(10,widthCanvas - widthImg),
//       random(10,heightCanvas - heightImg),
//       -0.1,
//       -0.1,
//       // random(-0.1,0.1),
//       // random(-0.1,0.1),
//       imageObjects[i],
//       widthImg,
//       heightImg
//     );
//     balls.push(ball);
//     // console.log(ball);
//   }
// }

// // define loop that keeps drawing the scene constantly

// function loop() {
//   // ctx.fillStyle = 'rgba(0,0,0,1)';
//   // ctx.fillRect(0,0,width,height);
//   ctx.clearRect(0, 0, widthCanvas, heightCanvas); // clear canvas

//   for(let i = 0; i < balls.length; i++) {
//     balls[i].draw();
//     balls[i].update();
//   }

//   requestAnimationFrame(loop);
// }

// loadImages(imagesSrc, createObject);
// loop();
  // complete: function(anim) {
  //   el.setAttribute('data-x', anim.animations[0].currentValue.slice(0, -2));
  //   el.setAttribute('data-y', anim.animations[1].currentValue.slice(0, -2));
  // }
  // update: function(anim) {
  //   move(el);
  //   xPos += 10;

  // }
// }).add({
//   translateX: x,
//   translateY: y,

//
// Interact.js
//

const targetInteract = '.nuage, .images, .textes';
const mediaQueryTouch = window.matchMedia('(hover: none) and (pointer: coarse)')
if (mediaQueryTouch.matches) {
  interact(targetInteract).draggable({
    allowFrom: '.zone',
    listeners: {
      start: function (event) {
        zIndexDrag += 1;
        event.target.style.zIndex = 100 + zIndexDrag;
        event.target.style.pointerEvents = 'none';
      },
      move: dragMoveListener,
      end: function (event) {
        event.target.style.pointerEvents = '';
      },
    },
    inertia: true,
    modifiers: [
      interact.modifiers.restrictRect({
        restriction: '.home',
        endOnly: true,
      }),
    ],
    autoScroll: false,
  });
} else {
interact(targetInteract).draggable({
  listeners: {
    start: function (event) {
      zIndexDrag += 1;
      event.target.style.zIndex = 100 + zIndexDrag;
      event.target.style.pointerEvents = 'none';
    },
    move: dragMoveListener,
    end: function (event) {
      event.target.style.pointerEvents = '';
    },
  },
  inertia: true,
  modifiers: [
    interact.modifiers.restrictRect({
      restriction: '.home',
      endOnly: true,
    }),
  ],
  autoScroll: false,
});
}

interact('.image--tete').draggable({
  listeners: {
    start: function (event) {
      zIndexDrag += 1;
      event.target.style.zIndex = 1000 + zIndexDrag;
      event.target.style.pointerEvents = 'none';
    },
    move: dragMoveListener,
    end: function (event) {
      event.target.style.pointerEvents = '';
    },
  },
  inertia: true,
  modifiers: [
    interact.modifiers.restrictRect({
      restriction: '.menu',
      endOnly: true,
    }),
  ],
  autoScroll: false,
});

function dragMoveListener(event) {
  let target = event.target;
  // keep the dragged position in the data-x/data-y attributes
  let x = (parseFloat(target.getAttribute('data-x')) || 0) + event.dx;
  let y = (parseFloat(target.getAttribute('data-y')) || 0) + event.dy;

  // translate the element
  target.style.webkitTransform = target.style.transform = 'translateX(' + x + 'px) translateY(' + y + 'px)';

  // update the posiion attributes
  target.setAttribute('data-x', x);
  target.setAttribute('data-y', y);
}

// this function is used later in the resizing and gesture demos
window.dragMoveListener = dragMoveListener;

if(navigator.userAgent.match(/(iPhone|iPod|iPad)/i)) {
  interact(targetInteract).on(['tap'], function (event) {
    const sonTarget = event.currentTarget.nextElementSibling;
    const audios = document.querySelectorAll('audio');

    closeBtn.classList.remove('none');
    musiqueBtn.classList.remove('none');
    containerOpen = true;
    allAnim.forEach((el) => el.remove(el.animatables[0].target));
    shuffleImages();
    shuffleTextes();
    for (let i = 0; i < audios.length; i++) {
      if (audios[i] != sonTarget) {
        audios[i].pause();
      }
    }
    if (!silence) {
      if (sonTarget.paused) {
        musique(sonTarget);
      }
    }
    nuage.blur();
    for (const nuage3 of nuages) {
      if (nuage3 != nuage) {
        x = (Math.random() < 0.5 ? -1 : 1) * randomNb(40,200);
        y = (Math.random() < 0.5 ? -1 : 1) * randomNb(40,200);
        move(nuage3);
      }
    }
  })
}

//
// Scrollbar with simplebar.js
//
let simpleBarContent = new SimpleBar(document.querySelector('.textes'));
// Disable scroll simplebar for print
window.addEventListener('beforeprint', (event) => {
  simpleBarContent.unMount();
});
window.addEventListener('afterprint', (event) => {
  simpleBarContent = new SimpleBar(document.querySelector('.textes'));
});

