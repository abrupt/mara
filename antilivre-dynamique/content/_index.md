---
sons:
- nom: "mara1"
  src: "/etc/mara1.mp3"
- nom: "mara2"
  src: "/etc/mara2.mp3"
- nom: "mara3"
  src: "/etc/mara3.mp3"
- nom: "mara4"
  src: "/etc/mara4.mp3"
- nom: "mara5"
  src: "/etc/mara5.mp3"
- nom: "mara6"
  src: "/etc/mara6.mp3"
- nom: "mara7"
  src: "/etc/mara7.mp3"
- nom: "mara8"
  src: "/etc/mara8.mp3"
images:
- nom: "mara1"
  src: "/img/mara1.jpg"
- nom: "mara2"
  src: "/img/mara2.jpg"
- nom: "mara3"
  src: "/img/mara3.jpg"
- nom: "mara4"
  src: "/img/mara4.jpg"
- nom: "mara5"
  src: "/img/mara5.jpg"
- nom: "mara6"
  src: "/img/mara6.jpg"
- nom: "mara7"
  src: "/img/mara7.jpg"
- nom: "mara8"
  src: "/img/mara8.jpg"
- nom: "mara9"
  src: "/img/mara9.jpg"
- nom: "mara10"
  src: "/img/mara10.jpg"
---

<div class="texte">

Non, je ne veux pas TUER LES FANTÔMES. Cette violence, il faudrait
qu'elle sorte nettement, comme la Dionée se ferme sur la mouche.

</div>
<div class="texte">

Je suis environné d'esprits, que je le veuille ou non. Alors j'invente
des scories VOLCANIQUES.

</div>
<div class="texte">

On fait de la poésie pour bébés, une VERSIFICATION PRÉNATALE et
nauséabonde. Māra salive la crasse de l'humanité, mais CETTE POURRITURE
n'a pas d'odeur, ni de parfum. Elle pue dans les têtes.

</div>
<div class="texte">

Māra, ton sourire purulent je vais en démanteler le SYSTÈME métrique
JUSQU'À L'OS. Cracher et me taire, ou ne jamais me taire.

</div>
<div class="texte">

Māra est sorti d'un morceau de canne à sucre. Māra avait trois filles
QUI ÉTAIENT TOUTES Māra. Elles dansaient le jour, la nuit... elles
dansaient jusqu'à la NAUSÉE.

</div>
<div class="texte">

Dieu, Dieu, Dieu... la lune va luire pour te nuire, alors mâche encore
des feuilles de sophora, C'EST MIEUX QUE la scammonée. Dans les
abattoirs les vaches, les veaux, tu sais ? On les égorge de SANG-FROID,
mon cœur...

</div>
<div class="texte">

Endormissement, PARASOMNIE, quand ils me parlaient, ce n'est pas à moi
qu'ils parlaient... Pendant que les autres PARLAIENT, MOI JE NE PARLAIS PAS, je collectionnais des escargots.

</div>
<div class="texte">

Aux quatre coins de la rose des vents s'en vont les ANIMAUX RAMPANTS.

</div>
<div class="texte">

Faire offrande, c'est se COUPER UNE MAIN.

</div>
<div class="texte">

Māra a toutes ses SAISONS en enfer. Māra se loge dans les têtes et dans
la poitrine. Il griffe la membrane OCULAIRE. Tout en souriant il se
DÉCROCHE LES DENTS, il te les enfonce dans les TYMPANS, il te vomit dans
les cheveux, te frictionne le crâne gaiement d'une MAIN CONVULSIVE.

</div>
<div class="texte">

Māra est une ordure, il veut que tu sois ordure. Sa vérité d'ordure veut
te briser, t'écraser LES VERTÈBRES, en faire des colonnes de douleur, à
en faire hurler les loups. Māra veut brûler la jungle, Māra veut jouir
DANS LE FEU.

</div>
<div class="texte">

Il y a tellement de feuilles qui sortent de tes yeux. Māra crachera dans
tes yeux pour les arroser... Māra affamé léchera ta nuque, caressera tes
aisselles comme s'il s'agissait D'OISEAUX FRAGILES.


</div>
<div class="texte">

Il arracherait bien la tête de ces oiseaux sortis du nid pour la mâcher
lentement, et surtout les yeux, car Māra adore manger des yeux, en faire
de la bouillie, un festin de tous les diables, un caprice DE MASTICATION...

</div>
<div class="texte">

Le mont *Bukhansan* est un refuge pour les fantômes et les esprits. J'y
ai vu Dakini danser sur un seul pied, puis ouvrir les cuisses et montrer
son VAGIN HUMIDE ET BLEU.

</div>
<div class="texte">

Dakini souriait avec des lèvres bleues et tout près d'elle un LOUP MANGEAIT L'AISSELLE d'une autre femme. DES CRÂNES brûlaient et des
flammes montaient des crânes et léchaient LES PIEDS de Dakini... Son
front était aussi UN SEXE BLEU, et la fleur sous son pied était une
flamme bleue... et sa danse était elle aussi une flamme bleue.

</div>
<div class="texte">

Māra *nightmare*, si on ne comprend plus rien à ce que tu dis, c'est à
cause du prurit de tes dents... tes filles sont des goules aux pieds de
vagins vahinés.

</div>
<div class="texte">

Māra que tu es laid... je suis renard, je maraude l'esprit léger... je
passe des heures les yeux fermés SANS SALIVER. Un chien dort sur du
fumier, c'est ma seconde naissance... DANS L'ACUITÉ ! Argos... la nuit
étoilée vibre COMME DES MOUCHES autour de tes yeux, viens que je te
lèche le visage COMME UN FIN LIMIER... tu sais bien que les dieux SONT DES MOMENTS de l'homme...

</div>
<div class="texte">

Le masculin Māra surjoue sa vigueur. Priape palefroi et palefrenier
tout à la fois, il donne DES COUPS DE BOUTOIR à te fracasser les reins.
Māra bande NERVEUSEMENT, ce trousseur de filles est en secret
COLLECTIONNEUR DE LARVES.

</div>
<div class="texte">

JE TE DÉSIRE Kusul, ma perle, bien AUTREMENT. Mon désir n'est pas mort
crois-moi, ton corps je le désire comme une fleur le MORO-SPHYNX.

</div>
<div class="texte">

Les infamies avaient conflué dans mon cœur comme des excréments dans des
latrines. Je décuple MA SAUVAGERIE, *subséquemment*... Je cesse de
parler et j'assassine, c'est le B.A.-BA... Je vais leur raboter les
paupières... à ces mânes aux doigts câlins. Ce n'est pas un simple
ornement, JE TOUCHE VRAIMENT DES FANTÔMES.

</div>
<div class="texte">

C'est le début de la fin, ou la fin du début, touche-toi les orteils
TROIS FOIS Māra et aspire à la mort spirituelle, dans un écho, un écho,
un écho...

</div>
<div class="texte">

Les plafonds deviennent tous DE LA BOUE. J'ai un tunnel dans la tête, on
y cherche des passages coûte que coûte...

Cette pénétration morale INSIDIEUSE est immorale... Faisons bouillir le
chevreau dans le lait de sa mère... POURQUOI PAS ?

</div>
<div class="texte">

Je partialise tes fesses Kusul et je crache sur la société DE LA PSYCHIATRIE. Où sont tes fétiches Māra ? Kusul, ton pied lui est
spirituel... Et je lécherai sur tes talons tout le POISON DE CE MONDE.
Je me fous bien des communautés, je n'aime que ton corps à toi, ET TON CON.

</div>
<div class="texte">

LES HOMMES AU POUVOIR se promènent et ne cachent plus leur érection.
Objet de psychiatrie, LA NORME-COÏT construit la clinique psychiatrique.
Les médecins ONT BAISÉ la monopathologie. Les articulations sont usées
*psychologiquement*. Māra, ton éthique normative DE CANCRELAT...


</div>
<div class="texte">

*Psychopathia sexualis*, mon œil ! quand ils parlent de perversion, de
dégénérescence... Nous sommes ASYMÉTRIQUES en amour Kusul, c'est sûr,
mais cette grande majorité d'hommes qui bandent dans les
cliniques-bordels, menaçons-les avec une faucille, on nous poursuivra
avec DES CHIENS ET DES DRONES...

</div>
<div class="texte">

Māra, viens un peu que je t'arrache les dents, que je les EXPULSE comme
de gros diamants, pour en faire des colliers de séton rouge. Je vais te
disséquer, séance tenante, SANS REGRET...

</div>
<div class="texte">

Et toi barbi·e barbante de l'inclusive, fille de Māra, goule
capricieuse, ta face sous toutes les coutures, on la voit trop souvent
DANS CE MANÈGE qui t'occupe vivement, les fruits des corbeilles
imprégnés des rides de tes aisselles, la poésie SOUMISE à tes dents
cariées.

</div>
<div class="texte">

Et toi, goule poétique sentimentale, as-tu lu quelque chose POUR QUE TA
CERVELLE suinte aussi fort la mort, et avec tant de ferveur ? Ton cœur
brisé en miettes mielleuses, ta face de vieille femme, tes cheveux DE
VEAU et ton corps tatoué jusqu'aux pis...

</div>
<div class="texte">

Les égéries purulentes, poète·sse·s de la TRANSGRESSION DES NORMES qui
font de la poésie une aberration normative. Je m'inquiète, la poésie,
elles l'ont soumise au déclin.

</div>
<div class="texte">

Kusul, nous faisons LABORATOIRE dans nos têtes depuis l'enfance. Tes
joues, ta mâchoire, je les embrasserais jusqu'à ne plus avoir de
souffle...

</div>
<div class="texte">

PAS DE LIEU AUTRE QUE LE LIEU SANS LIEU. Kusul, je voudrais te parler de
vie surnaturelle. SOUS TES YEUX, Māra est DÉGLUTI. C'est un fait,
l'oiseau va vivre en lui becquetant les yeux.

</div>
<div class="texte">

Regarde bien mes yeux Kusul, cerclés de noir, DE POINTS ROUGES. Je
pourrais crever de rire dans les rues de Séoul. Un jour, la terre
s'est mise à trembler. Même MES LÈVRES remuaient !

</div>
<div class="texte">

Et voilà que je me casse le nez. LE SANG INONDE LA CHAMBRE. Dorénavant,
le réel n'est plus IMMACULÉ. Kusul, veux-tu un peu de rhinoplastie ?
L'OS PROPRE de mon nez me fait un nez busqué, je suis pâle comme un
RENARD...

</div>
<div class="texte">

Kusul et ta mémoire... *La descañona*, elle l'écorche... MAUVAISE
NUIT, bien mauvaise, à vomir, ni plus ni moins. *Le sommeil de la
raison* en quelque sorte... Chauves-souris et hiboux, un lynx dans le
coin droit, et des BÉBÉS MORTS dans des paniers tressés. LES FEMMES
SUCENT les gamins jusqu'à la moelle. Suivez les conseils du Grand Bouc
! *Volaverunt*...

</div>
<div class="texte">

Kusul, je veux faire *Zazen*. Parce que ma CERVELLE a trop vrillé. Kusul
COLLE TA LANGUE sous ton palais humide. Tes doigts, tes mains
s'immobilisent. L'air vibre comme une flamme bleue. Ton cœur ne saute
plus COMME UN LAPIN...

</div>
<div class="texte">

Ne penses-tu pas que tout était PRÉVISIBLE ? Portez vos
énormes CORNES DE LICORNES antiques... on vous ouvrira la bouche, on
vous fera la chasse aux dents. C'est magnifique cette sanguine, cette
face DE PENDU, quelle volupté du tracé ! Quelles fesses de marbre ! On
voudrait les palper, les mordre. Brûlons la MAISON COCOTTE. Ils s'en
iront déplumés, ils s'en iront *Zazen* les sortilèges, la bave aqueuse
de Māra, tu vas vite flairer l'épine PETITE HERMINE, et traverser les
mers.

</div>
<div class="texte">

Capturer des fantômes, est-ce bien sérieux Kusul ? En position DU LOTUS,
avec mon nez cabossé ? On fait de la poésie dans des livres cousus main !
Cette ALIÉNATION DE LA LANGUE, leur face farcie d'eau. Quels
passionnants résultats ! Bête originale, au paroxysme de la crise,
j'éternue !

</div>
<div class="texte">

Pointe d'une aiguille d'acupuncture que l'on m'enfonce DANS LES JOUES,
ou sur la langue ? Non, plutôt dans les genoux ! Ou sous la voûte
plantaire... PLANTAIRE !

</div>
<div class="texte">

Respiration ABDOMINALE abominable, comme le vent souffle ! Il fait
remuer les branches... Les branches remuent comme des os souples. Des
POULPES SOUPLES...

</div>
<div class="texte">

Des fleurs en intraveineuse Kusul, elles fleurissent dans ton sang...
Des fleurs blanches comme des GLOBULES EN GRAPPES... Des reliquaires
mous !

</div>
<div class="texte">

Est-ce que ces fleurs de sang pourraient guérir LES FIBROSES, calmer,
fleurir sur les nerfs ?... Les fleurs, Kusul, fleurissent simplement...
Écoute comme le hautbois sonne HAUT DANS LES BOIS. Peut-être que des
serpents dorment sous les planchers, dissimulés dans l'ombre, fuyant les
humains COMME LA PESTE.

</div>
<div class="texte">

Peut-être que se sont les serpents qui jouent du hautbois ! L'humain est
si occupé, plongé dans SA MORBIDITÉ quotidienne, asphyxié DANS LE
NÉANT... Serpents, crachez... *Sam xiag, sam xiag, sam xiag*... Crachez,
mais surtout... NE TOUCHEZ PAS à cette main humaine ! Kusul, toi aussi,
fuis l'humain...

</div>
<div class="texte">

Il faudrait donc entrer en résonance avec son époque, SON NAUFRAGE ?
Mais non, mais non... Quand le sang coulait de mes narines, j'essayais
de retenir le LIQUIDE HÉMATIQUE dans le creux de ma main, mais il y en
avait trop ! Que voulais-je retenir à la fin ? Ce n'est pas sérieux ? On
ne peut ASSUJETTIR le sang !

</div>
<div class="texte">

Kusul, tu te mettras à prier, qui sait, devant les images sculptées de
*Sansin*, divinité de la montagne, accompagnée de son tigre AUX YEUX
HALLUCINÉS...

</div>
<div class="texte">

Ne perds pas l'espoir DE TE PERDRE. Tu l'as bien vu quand tu as fermé
les yeux, le tigre noir s'évapore. Tu lui as offert quelques fraises de
tes mains fiévreuses. SES POILS se hérissaient, puis il a ouvert LA
GUEULE pour te parler :

*L'araignée serra le cœur et le cœur serra l'araignée. Le cœur et
l'araignée s'aimèrent et l'araignée et le cœur ne se quittèrent plus.
L'araignée caressa le cœur de ses huit pattes fragiles et le cœur
réchauffa l'araignée de son sang rouge. Il n'y a pas de piège dans cet
amour-là*.

</div>
<div class="texte">

Pour sûr Kusul, ce tigre te ressemble, il parle LA MÊME LANGUE QUE TOI.
Peut-être que cet animal, comme un double FANTOMATIQUE, te parle parce
que TU ES MUETTE.

</div>
<div class="texte">

Aujourd'hui, tu regardes les feuilles qui remplacent les feuilles.
Personne n'enlève LA PEAU DES CHOSES, tu le sais très bien. Gautama est
silencieux. Pourquoi vouloir à tout prix faire sens, ou faire trembler
L'ŒIL DU LYRISME ?

</div>
<div class="texte">

Tu préfères tisser, gonfler la tapisserie de ses motifs végétaux,
animaux comme la langue secrète ÉRUCTÉE dans le coquillage.

</div>
<div class="texte">

Mais ma parole, Kusul, tu parles ! Mais oui tu parles et tu commences à
mettre cet alphabet DANS TA BOUCHE de fantôme. *Hangeul*. Peut-être
as-tu une spiritualité débordante, un MONUMENT INFINI dans la
cervelle...

</div>
<div class="texte">

Māra, ta bouche pâteuse aux relents de vin espagnol... mes yeux
tremblent à ce mot, SOCIÉTÉ... alors... y FOUTRE LE FEU, une bombe
artisanale s'il le faut, allumer un incendie sur la SOCIÉTÉ BLANCHE,
INTELLECTUELLE, les saigner, les auteurs poétiques du partage
D'IMMONDICES.

</div>
<div class="texte">

Kusul, détends-toi un peu... Ça t'apaise de poser ton regard sur LE
SOMMEIL DES CHIENS. LE MOUVEMENT AUTONOME DU VIVANT n'a besoin d'aucune
parole pour UNIFIER. La vie devenue visible, HYPERVISIBLE est une
négation de la vie. Il faut donc défendre L'INVISIBLE.


</div>
<div class="texte">

Tu ne veux aucun mal, AUCUNE PLAIE. Plus d'hôpitaux, D'EFFONDREMENTS, de
punaises, de poissons d'argent (LÉPISME), de cafouillage, de tombeaux,
d'engloutissements sociaux... Avec le vent, TOUT DEVIENDRA HORIZONTAL.

</div>
<div class="texte">

Au lieu de colonies D'ICEBERGS, réduis ta pensée à la taille d'un
flocon saisonnier. Ce qui COAGULE n'est que formulations négatives...
Les éclairs sont des mondes qui entrent dans le monde.

</div>
<div class="texte">

Métaphysique de Kusul. Kusul pense et écrit : *Le romantisme d'époque
s'agite dans une FANTASMAGORIE RÉVOLUTIONNAIRE ET IDIOTE. Son
impuissance est contenue dans ses dithyrambes d'un siècle révolu et qui
vibrionne SANS SUBSTANCE comme des relents de dialectique dans une
sphère numérique sans fond.*

</div>
<div class="texte">

*Son manque de substance s'accorde à la NÉBULISATION de l'information
et du langage. Son vice et sa faiblesse élaborent une pensée rétrograde
qui annihile TOUTE FORCE véritablement créatrice et condamne l'homme à
graviter dans un IMMOBILISME délétère et stérile.*

</div>
<div class="texte">

*Le monde poursuit ainsi SES MUTATIONS DÉVASTATRICES, celles ordonnées
par une société ultrapositiviste dont le moteur de PRODUCTIVITÉ est
alimenté par UNE CHAMBRE DE COMBUSTION d'où se désagrège et brûle la
volonté créatrice d'émancipation.*

</div>
<div class="texte">

*C'est la destruction de toute volonté HUMAINE ET CRÉATRICE qui met en
branle la société ultrapositiviste. Les pseudo-révoltes sont les
ALIMENTS INOFFENSIFS du monstre carnassier. L'anéantissement de la
pensée est son principe de PRODUCTION ÉLÉMENTAIRE. Les cervelles sont
les combustibles pour l'effacement même de la pensée.*

</div>
<div class="texte">

On y foutrait le feu Kusul, à ce monde qui sent LA PISSE DE BŒUF. Les
racines sèchent et se brisent comme des os canins. LA VIE SE FOSSILISE.
Alors, qu'en penses-tu de ce monde en ruines ? Tu sais, moi je ne vois
pas les ruines... Je suis sonné, le COUP RÉSONNE sorcière, mon
secret... la *mètis*... chien fourbe, panthère ou renard rusé. Je me
dissimule, j'affuble en esprit... c'est la conduite DE CELUI QUI CONNAÎT
LA VÉRITÉ.

</div>
<div class="texte">

Mais avant de mettre ne serait-ce qu'un seul pied dans le sanctuaire...
Prenons un bon BAIN DE PIEDS ! Et lavons-nous LES GLOBES OCULAIRES à
l'acide ! Parce que nous allons voir des tortues noires QUI S'ACCOUPLENT
avec des serpents tortueux. Leurs langues ne formant qu'une seule flamme
dans un baiser en BRASIER ROUGE sang.

</div>
<div class="texte">

Nous verrons aussi... les mutations rouges, les corps en fusion, les
oreilles en plumes, LES CARTILAGES NOUEUX. Les créatures se
métamorphoseront DANS LE FEU. Vraiment !

</div>
<div class="texte">

La langue est une chambre de combustion où le feu régénère le monde,
elle le PURIFIE. Vous qui puez comme on PUE DU CRÂNE, vos pensées
s'écroulent, il ne reste plus que vos yeux qui fixent le jour, vos
gorges, ou des NOMBRILS CICATRISÉS, purulents. Vous ÉGORGEZ les animaux.
Vos pensées sont laborieuses, elles sortent d'un trou terreux où
macèrent LES RACINES D'AGAVE.

</div>
<div class="texte">

Oui Māra, tu MORDS L'AGAVE, jusqu'à te teindre les gencives D'UN VERT
AFFREUX... à se tordre de rire, oui... Le vert d'agave dans TA BOUCHE
TERREUSE.


</div>
<div class="texte">

Longtemps, très longtemps, j'ai eu un esprit lunaire, dévoré,
DÉVORANT...mais un jour je me suis mis à aimer LES PAGODES de pierre,
sans raison, inconditionnellement, irrationnellement.

Je rêvais des tombes de *Goguryeo*, D'UN BESTIAIRE DE SANG, des gardiens
de l'enfer AUX YEUX de lotus. Le serpent tortueux, tu ne le vois donc
pas ? C'est le nerf lui-même, ou LA DÉCHARGE NERVEUSE.

</div>
<div class="texte">

Je fermais les yeux, je les voyais ces fleurs ÉCLORE DANS L'OBSCURITÉ.
Où est-elle ton harmonie, renard doux, PANTHÈRE DES SABLES ? Doux renard
de LA VIDUITÉ. Je restais à l'intérieur, car l'extérieur N'ÉTAIT PAS
FIABLE. À cultiver mes pensées, comme de l'agave.

</div>
<div class="texte">

Kusul pense encore : *L'homme est retenu par sa pensée sur l'homme, et
il n'en sort pas*. Mieux vaut ne pas avoir LE CŒUR TROP FRAGILE, Kusul,
sinon clairement tu te feras bouffer. Ne vois-tu pas qu'ils sont
incapables d'écouter, qu'ils agissent ou ne réagissent qu'en fonction de
RESSENTIMENTS animés par leur esprit de mort ? Leur existence est vidée
de toute substance OU MÊME DE VOLONTÉ. Ils ont besoin de t'exclure,
comme ils excluent LA RÉALITÉ DU MONDE. Ils sont dépourvus d'imaginaire
ou d'intuition.

</div>
<div class="texte">

Leurs rêves sont rares et absurdes. Sinon des rêves de VENGEANCE... Ils
peuvent te détester comme au fond ils se détestent EUX-MÊMES, jusqu'à
S'AUTOMUTILER ! Se détester est encore une conséquence de leur
NARCISSISME PATHOLOGIQUE.

</div>
<div class="texte">

Ils détestent le monde et les autres, mais ne sont JAMAIS RIEN sans
cette détestation des autres. Cette pathologie nerveuse est invisible au
premier abord, bien que LES YEUX LES TRAHISSENT, quand ils se figent,
CETTE GELÉE OCULAIRE...

</div>
<div class="texte">

Kusul, cette gelée oculaire, tu la vois dans les regards figés par LA
TORPEUR, dans leurs yeux hagards... Ils semblent te mitrailler de leur
sempiternel questionnement : QUI SUIS-JE ? Dis-le-moi je t'en supplie,
toi, que je ne connais pas, que je ne pourrai jamais connaître et qui
n'existes que pour me dire ce que je suis... J'AI BESOIN DE TOI, pour me
dire ce que je suis. Je ne peux pas le faire SEUL·E. Quand je suis
seul·e, JE N'EXISTE PAS. Ma solitude ne m'apprend rien. Et
malheureusement, cette MÉCONNAISSANCE DU MONDE, en dehors de ma sphère
personnelle et vide, m'ennuie. Je m'ennuie...

</div>
<div class="texte">

Tant pis pour eux. Au fond, les MIROIRS NOIRS combleront d'images ce
vide existentiel profond. Leur regard sera *fixé*. Leur regard sera dopé
AU VIDE. Ils sont dans les nœuds de la mort, *Tssing Tssing*, Kusul
décoche une flèche... tu es vraiment belle avec ton arc, ton ŒIL
MEURTRIER... et ta vengeance qui ÉMASCULE. Vas-y ! Chasse crûment !
Sonde mon cœur si tu veux, et TORPILLE LA GLAIRE.

</div>
<div class="texte">

As-tu remarqué Kusul comme la langue peut circuler et prendre à
contre-pied CETTE INERTIE du langage ? C'est un contrepoint qui orne,
comme une veillée dans le vivant, et qui vient OCCULTER la torpeur
ANKYLOSÉE des névropathes.

</div>
<div class="texte">

Tu as beaucoup pensé à ça Kusul, et tu continues : *La reconnaissance
intuitive est UNE RUPTURE, à la fois faille et éveil. Dans
l'insignifiance du langage, elle devient SIGNIFIANT APORÉTIQUE.*
Il faut tenter une pensée ni ascendante ni descendante, mais une
DIALECTIQUE DES OS, du grain et de la moelle : une MÉTAPHYSIQUE
INTRACORPORELLE. Ta métaphysique voudrait définir cette intuition de
vie dissimulée et invisible, comme une énergie organique qui t'anime et
qui peut être SOURCE DE RÊVES ou de réminiscences de vies antérieures.

</div>
<div class="texte">

Ta colonne vertébrale est UN TOTEM d'où jaillissent des couleurs
irisées. CE SIGNIFIANT reste toujours à la limite, à la frontière de la
perception et se dérobe à la faculté des sens. Cette matière sans fond,
comme L'ANTIMATIÈRE D'UN SONGE, *incarne* le mystère en se refusant à la
MORTIFICATION DU LANGAGE et des images. Tu en perçois le flux intérieur
et mouvant dans un espace indéfini et intime.


</div>
<div class="texte">

Vie intérieure ?... Sûrement plus que cela... Vie SURNATURELLE ! La
mémoire remue comme le dos d'un poisson... Cette mise en branle SECOUE
ton être, tes pensées deviennent POREUSES et s'interpénètrent au sein
d'un même présent QUI BRÛLE. Kusul, tu es habitée par tes rêves...

</div>
<div class="texte">

La langue s'accélère et s'accélère, ou bien EST-CE LE CŒUR ? Cette
sensation, une voix qui monte et qui monte dans la tête. J'ai vu passer
l'automne à la vitesse d'un AVION DE GUERRE. Ma tête crépitait dans les
bruits des moteurs, et je voulais parler seul... Je renâcle ? Mais avant
l'*homophrosunè*, il y a le meurtre des prétendants, le SANG NOIR qui
coule sur les dalles de la maison et les gorges qui crachent leur mort.
Dans les râles, tu m'enlaces Kusul, ma perle, même si je pue LE SANG ET
LA SUEUR.

</div>
<div class="texte">

Les apitoiements pour la masse aveugle DES AVEUGLES. *Papaver,
papaver*... Joyeux jardin des bulbes, danse des trombones et des
cuivres... Tubas et glaives à la fois... On peut voir SATURNE cette nuit
et ses sœurs Alcyonides... C'est toi Kusul qui me la montres du doigt...
Puisque nous sommes tout juste AU SOIR DU SOLSTICE, nous mangerons une
soupe de haricots rouges !

</div>
<div class="texte">

Il y a un vent salubre qui nettoie des alluvions du passé. Ce vent, je
le sens, il me foudroie, je le reçois EN PLEIN VISAGE. Le regard n'a
jamais été émoussé. Dans mes yeux, il n'y a pas d'erreur. Nous aurons,
Kusul et moi, des yeux plus grands que des soucoupes. Parce qu'il y a un
vent de TOUS LES DIEUX.

</div>
<div class="texte">

Je t'écoute parler Kusul... ce langage m'enchante... LES RHIZOMES se
déploient à vue d'œil. Les branches montent géométriquement. Les fruits
mûrissent. Alcyon couve ses œufs EN SILENCE, puisque nous venons juste
de passer le solstice. L'œuf et le cœur, CES NOYAUX CALMES, bien loin de
Māra.

</div>
<div class="texte">

J'ai un bambou affuté dans les mains... AFFUTÉ ! Tu auras double dose
Māra, si je te croise, je te planterai le bambou DANS LA GORGE, pour que
ton sang inonde le sol et qu'il te monte à la tête. La plaie sera
immense, ce sera un joli TROU NOIR, comme l'écorce du crime qui accroche
L'ÉPIDERME. Élection divine d'un démon ! Tes yeux te sortent de la tête.
Tire la langue, que JE TE LA TRANCHE ! Cette langue, empoisonnée comme
une feuille de sang, je la jette au sol et je l'écrase, je la piétine,
comme UNE ÉPONGE DE VIANDE. Cette sangsue humaine, poison et source de
poison.

</div>
<div class="texte">

J'en ai trop vu de ces goules mâcher puis roter leurs coquillages. Et
vous, vous ne voudriez pas angoisser FACE À LA MASTICATION des
coquillages ? Les pavots étoilés s'ouvrent comme des giroflées, LEURS
YEUX SONT DES POINTS CREVÉS. Il faudrait dormir Kusul, main dans la
main, et parler comme DES ENFANTS DYSLEXIQUES. *Anansi*, je te mangerai
la tête le premier, je te ferai cuire des ormeaux et des coquillages de
saison pour tes mâchoires endolories. MAGIE DU MEURTRE dans les champs
d'Amathonte, *mais tout change et rien ne meurt.*

</div>
<div class="texte">

Je tiens le divin bambou. Je vais te l'enfoncer dans les cavités
oculaires, faire sortir de ton ventre UNE COLONIE DE PAPILLONS. Sur les
toits d'émail, ON SACRIFIE des oiseaux, des plantes, des tigres. ON
BRÛLE LEURS YEUX, on lisse leurs plumes dans des bassins de terre et
d'argile. On remuera des cils, on glacera LES VENTRICULES ET LES ARTÈRES
CORONAIRES, tu ne crois pas ?

</div>
<div class="texte">

Dans les cages thoraciques, dans les courants d'air, qui les remarquera
cette fois Kusul, tes baisers *halal*, ta FRILOSITÉ MENTALE ?

</div>
<div class="texte">

Je joue des gammes SUR DES OS DE COBRA, j'ai ouvert mon cœur je te jure,
en deux ou trois : il était frais, il était beau comme un fruit avec ses
graines de pavot, SES ÉCAILLES DE TRUITE et ses alvéoles tirées au
cordeau. Phaéton, fils idiot, QU'ON T'IMMOLE, qu'on te plante des
couteaux dans les cuisses, esprit de graisse et de cire, qu'on te brûle
paille-en-queue JUSQU'À LA MOELLE.

</div>
<div class="texte">

Māra va sucer les cornes des vaches cornues jusqu'à ce que NOS LARMES
S'ÉVAPORENT. Mentalement, je me calme en nourrissant les anguilles du lac
Pergus, en nourrissant les carpes du lac Averne... Mais que vois-je ? On
te coupe la gorge ? On te renverse DANS LA TOURBE, dans le sang. C'est
mérité *Lycaon*, MÂCHEUR DE DOIGTS, singe hurleur et mangeur de
hachis... de DAWAMESK, on te gave, mais rien n'apaise ta fièvre.

</div>
<div class="texte">

Kusul, jouons tous les deux allongés sur des tapis de fibres, à chercher
puis à trouver des fleurs DANS LES MOTIFS, je te dirai ce que je vois...
Je t'invite à bâfrer une bonne salade d'amome, je sais COUPER les
feuilles en fines lamelles, c'est ma spécialité, de MANIER LE COUTEAU,
de hacher menu les végétaux pour en retirer le suc, l'amertume...


</div>
<div class="texte">

Je caresse TES PHALANGES Kusul, c'est une évidence, ton sang qui
circule entre les os saillants et se verse comme le courant d'une
rivière ENTRE LES PIERRES... Mais voici les ombres qui s'avancent, LES
YEUX CIREUX et tristes comme des poulpes crevés sous le poids des
tentacules qui sonnent le glas, des testicules tatoués ou des anneaux
nasaux, quand ON ÉPINGLE LES PAPILLONS pour faire des partitions
célestes, mais moi J'AIME TES OS.

</div>
<div class="texte">

La psychologie du philosophe s'atrophie sous psychotropes... et acidifie
mes vertiges... Kusul, toi seule tu adoucis les angles de la géométrie,
et TU SUCRES LES FRAISES par poignées fébriles SUR LES TÊTES IMBÉCILES,
ceux qui baignent dans l'esthétique de la neurasthénie.

</div>
<div class="texte">

Kusul me raconte une histoire :

*Au bord du fleuve, un singe mange des organes. Le sang lui barbouille
la face en rouge solaire. Les babouins embrassent les antilopes, défiant
la mâchoire du crocodile, mais où sont donc passées les fleurs ?*

Et puis une autre :

*Deux pastèques et deux rats qui s'approchent en silence dans la terre
et crèvent les pastèques le jus dégouline sur la terre et sur les dents
des rats qui mordent dans les pastèques qui se crèvent et se vident les
rats mâchent les grains de pastèque dans la nuit on entend le bruit des
grains qui éclatent dans la nuit et les yeux des rats sont aussi des
grains noirs comme ceux de la pastèque la lune brille dans la nuit et
brille aussi dans les yeux noirs des rats et les pastèques coulent et le
jus rouge se déverse sur la terre et les pattes des rats
s'enfoncent dans la boue rose et sucrée l'un des rats monte sur le dos
de l'autre rat pour forniquer dans la nuit sous la lune qui brille dans
le ciel et dans les yeux des rats mais le rat forniqueur se fait mordre
par l'autre rat qui veut continuer de creuser la chair sucrée de la
pastèque qui coule sur la terre humide mais l'ombre d'un chien errant
apparaît sous la lune alors les deux rats s'enfuient dans la nuit le
chien se met à renifler la pastèque et les traces laissées dans la
terre humide par les rats il lèche la chair de pastèque tombée sur le
sol et creuse la terre et pisse sur les pastèques et les feuilles de
pastèque.*

</div>
<div class="texte">

La scolopendre tu vois, c'étaient mes propres doigts qui cherchaient
QUELQUE CHOSE À AIMER... la scolopendre tu vois, c'étaient mes propres
doigts qui pondaient leurs œufs sur la tige d'un rosier... Māra roule
des yeux terribles, sa mâchoire rumine une colère rouge. Māra, c'est le
bruit de l'époque. Māra, c'est LE VENTRE des images. Ses filles sont
obscènes, non par le corps, mais par LE MASQUE de la tromperie et des
idées rances. Il y a de la fatigue dans leurs yeux, posés sur une bouche
qui parle seule.

</div>
<div class="texte">

Les goûts douteux, LA MASTICATION qui creuse la joue. Les filles de Māra
tournent sur elles-mêmes COMME DES VERS. Nous sommes bien loin d'une
communion spirituelle. Elles prennent une figure de circonstance, afin
de creuser UNE TERREUR ÉMOTIONNELLE. Elles salivent sous les masques de
la musique et de la danse. LEURS NARINES SÈCHES se dilatent. Elles
veulent provoquer LA MORT SPIRITUELLE, toucher au corps vide,
désincarné, morbide.

</div>
<div class="texte">

Mais toi, qui voudrait véritablement entendre ce que tu as à dire, Kusul
? Qui veut encore entendre ta voix ? ET POURQUOI PARLER ? Tu ne veux pas
de cette parole qui se déverse JUSQU'À LA CÉPHALÉE. Elles se sont donné
des noms de fleurs, mais tu le sais très bien... les fleurs véritables
sont LES FLEURS MUETTES. Et les filles de Māra crient, mais ignorent que
tu ne les entends pas... Voudraient-elles ENSEMENCER LA MER, là où les
graines ne peuvent germer ?

</div>
<div class="texte">

Dans les filles de Māra, n'oublie pas que c'est LE MASCULIN MORBIDE QUI
S'INCARNE SEUL. C'est la pensée sans voile, le rire de LA MASTURBATION
ÉLECTRIQUE. Māra te masturbera de force, plongera ses mains entre tes
cuisses Kusul, il HUMERA ton sexe comme s'il lui revenait DE DROIT. Sa
langue glisse sur ses lèvres. Les filles de Māra meurent de faim en te
dévisageant avec LEUR SEXE QUI EST AUSSI UNE BOUCHE. Le voici, l'amour
surdoré de la digestion stomacale. Ton ANÉANTISSEMENT est leur
nourriture.

</div>
<div class="texte">

Leurs seins en pointe ne sont que LE MEMBRE ÉRECTILE et vert de Māra
leur père. Elles veulent sucer tes doigts comme elles suceraient la
mort, d'une succion affamée, prêtes à mordre. À DÉCOUPER LA CHAIR. Ton
anéantissement, leur nourriture... Cette panique, CES RÉVULSIONS du
corps, Kusul, tu aurais pu les prendre pour une vérité, parce qu'elles
te sautent au visage, alors que la vérité, elle, sait patienter. Les
filles de Māra ne sont que des *images*, des perturbations dues à Māra,
conséquences de son existence MALHEUREUSE dont il fait UNE PRÉDATION...
La prédation est sa réponse au malheur. Il cherche ainsi UNE PROIE pour
que la mastication du vivant COUVRE LE SILENCE de sa mort.

</div>
<div class="texte">

Mais le silence de mort n'est pas identique AU SILENCE DU VIVANT. Le
silence peut manifester toutes les strates du vivant (liquides,
aériennes, terrestres...) ET TU LE SAIS. L'immobilité contient toutes
les directions possibles du mouvement. L'IMMOBILITÉ PEUT RÉSISTER au
silence de la mort. Elle stimule silencieusement la racine, LE SUC
INTERNE des floraisons. Elle fonde une force, une puissance invisible
contenue dans le cœur du monde, c'est-à-dire DANS LE VIDE. Qu'on puisse
la nommer... peu importe, puisque cette essence DÉFIE LE LANGAGE et la
reconnaissance des sens. Si l'on peut espérer assimiler son rythme
périodiquement, rien ni personne NE PEUT LA CONTRAINDRE. C'est un cœur
sans matière, un cercle qui n'a pas de centre, ou UNE SOURCE SECRÈTE,
dissimulée mais omniprésente. C'est à la fois LA PEAU DE TOUTE CHOSE,
son contenu matériel et sa substance immatérielle...

</div>
<div class="texte">

Plus que les corps, les ombres retiennent ton attention Kusul. On a
voulu te faire croire que les chauves-souris étaient des créatures
maléfiques. IL N'EN EST RIEN, et tu peux les chérir, les caresser, fixer
leurs yeux noirs SANS AVOIR HONTE. De toute façon, il y a bien longtemps
que tu ne fais plus partie de cette humanité adepte DE LA MASTICATION.
Mais de quoi est fait ce regard, celui qui te donne à voir UN MONDE QUI
ENTRE DANS LE MONDE ? Où places-tu ton *locus amoenus*, Kusul ? Dans le
ciel, je crois...


</div>
<div class="texte">

Jamais plus elles ne verront ton visage, ces sorcières des rites. CES
SUCEUSES DE VERTÈBRES. Pourrais-tu être sensible Kusul au chant des
oiseaux et oublier l'odeur de la viande sanglante des bébés cuits DANS
DES RIVIÈRES ? Leurs danses macabres, leurs rires mortifères ?
Aujourd'hui, le ciel était clair comme de l'eau DANS UNE BASSINE.

</div>
<div class="texte">

Je ne cille pas. Oh, mon Dieu, je ne pleure pas. Surtout pas de ça avec
moi ! Je suis hanté. Je nous revois autour de cette table, entre ces
murs. COUPE-MOI LA TÊTE. Arrache-moi cette face de rat ! Ma langue fait
TROP CACTUS, ma langue de reptile. À la place, j'aurais dû, si j'avais
su, si j'avais su... m'enfuir. Quitte à m'envoyer des doses de
mescaline, ou faire pousser de l'agave dans une serre, TESTER DES
DÉCOCTIONS pour tout noter dans un carnet. J'aurais eu comme ça les yeux
grands ouverts. Ouverts sur des couleurs, DES SYMÉTRIES FOLLES des
infinis multipliés. De belles tapisseries en définitive.

</div>
<div class="texte">

Une cabane, un mirage... Elle grouille de sauterelles noires... RIEN N'Y
TIENT lieu de refuge. Ses parois tremblent. Cabane de boue, de courants
d'air toxique. Les paroles grasses y infusent. C'est LA TORPEUR DU
BANAL, des dates, des calendriers, des éphémérides bientôt ridés,
bariolés, DE CUIVRE plus que d'or. On y glousse, on y tousse grassement
les glaires laborieuses. Le plafond suinte de dégoût, la pluie
insalubre. Les cheveux humides DE SUEUR CAPILLAIRE plus que de pluie.
Les araignées se décomposent dans les fumées. Non, ce n'est pas un
refuge, la cabane dégringole, et j'échappe de justesse À L'ÉCRASEMENT,
en passant par la fenêtre...

</div>
<div class="texte">

Je te jure Kusul, c'est magnifique, le lion d'Yvain leur saute au
visage, LES DÉFIGURE... d'un coup de mâchoire emporte le flanc d'un
homme. Les filles de Māra seront aussi TUÉES par un lion dans mon rêve,
croquées jusque dans les viscères... moi, je les finirai à l'arme
blanche.

</div>
<div class="texte">

Parce que je reste assis immobile et que je ne dis rien et que JE NE
PENSE PLUS, si ce n'est à une manière de ne plus penser et de ne plus
regarder, ne plus écouter sinon L'AMPLEUR d'un souffle qui s'accorde au
silence... Māra, le silence, l'immobilité sereine, ça te tue.

</div>
<div class="texte">

Je me fous du cosmos, de l'Olympe, du mont des Oliviers, mais peut-être
pas tellement du FIGUIER DES PAGODES. J'essaie de tenir mon délire, DE
DÉLIER le spirituel, de faire vibrer la dentition.

</div>
<div class="texte">

Il faut, pour parler de ça, l'avoir connue cette brûlure dans la
poitrine, ce resserrement dans l'abdomen. NOUS TOURNONS
ELLIPTIQUEMENT dans ce monde Kusul, et nous revenons aux saisons qui ne
sont jamais tout à fait les mêmes. Et nous non plus, nous ne serons
jamais tout à fait les mêmes. C'est en cela que CETTE VIE EST PARFAITE.
Tu dois aimer les cercles, les cycles, l'elliptique. Tout peut renaître
dans ton cœur, si tu gardes cette volonté parfaite.

</div>
<div class="texte">

Ceux qui sont unis... SANS RAISON... n'ont aucune raison pour se
séparer... Ne cherche pas à me comprendre... Nous sommes des mystères...
*plus haut que l'amour des hommes est l'amour des choses et des
fantômes. Ce fantôme qui court devant toi, il est plus beau que toi ;
pourquoi ne lui donnes-tu pas ta chair et tes os ? Mais tu as peur et tu
cours te réfugier auprès de ton prochain.* J'aurais donné ma peau pour
DES FANTÔMES DE QUARTZ ! Pour des fantômes de sang, des yeux de
fantômes, des fantômes de fleurs...

</div>
<div class="texte">

Nous sommes toujours des fantômes EN RÊVE. Nous ouvrons nos yeux de
fantômes sur LA NUIT INTÉRIEURE.

</div>
<div class="texte">

Je te reparle des *gui* Kusul, ces fantômes errants, ces ombres noires échevelées. J'ai vu un *gui* un jour, ALORS QUE J'ÉTAIS ENFANT, un après-midi ÉTRANGEMENT SILENCIEUX... j'étais seul dans un jardin, je me tenais debout face à cette SILHOUETTE NOIRE. Ses cheveux électriques semblaient danser. Je me souviens très bien de ses yeux ronds qui me fixaient comme deux points blancs. Pourtant, JE N'AI PAS EU PEUR... Dans le silence, le fantôme, TOUT EN ME FIXANT avec ses yeux blancs, s'est mis à me sourire, au milieu DES FLEURS DE COSMOS.

</div>
